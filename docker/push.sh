#!/bin/bash

cd $(dirname $0)    
. docker.conf         # 設定読み込み

sudo docker login ${REGISTRY_PATH}

TAG_NAME=${1}
echo "### Push ${TAG_NAME}"

sudo docker push ${REGISTRY_PROJECT_PATH}${TAG_NAME}
