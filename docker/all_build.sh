#!/bin/bash

cd $(dirname $0)    
. docker.conf         # 設定読み込み

for tag in ${TAGS[@]}; do
    . build.sh ${tag}
done
