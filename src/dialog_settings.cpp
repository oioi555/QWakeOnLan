#include "dialog_settings.h"
#include "ui_dialog_settings.h"

#include <QListWidgetItem>
#include <QMessageBox>
#include <QProcess>
#include <QStyleFactory>

DialogSettings::DialogSettings(QWidget *parent) : QDialog(parent)
  , main(qobject_cast<MainWindow*>(parent))
  , ui(new Ui::DialogSettings)
{
    ui->setupUi(this);

    // window
    setWindowTitle(tr("%1 : Settings").arg(qApp->applicationName()));

    // list menu
    static const QList<menuItem> menuItems = {
        {tr("General"), "configure"},
        {tr("Ping"), "exception"},
        {tr("RDP link"), "link"},
        {tr("Suspend"), "system-suspend"},
        {tr("Scanner"), "edit-find"},
        {tr("Help"), "help-about"}
    };

    foreach (auto menu, menuItems) {
        auto *item = new  QListWidgetItem(menu.name, ui->listWidget);
        item->setIcon(QIcon::fromTheme(menu.icon));
    }
    connect(ui->listWidget, &QListWidget::currentRowChanged, [=] (int currentRow) {
        ui->stackedWidget->setCurrentIndex(currentRow);
    });
    ui->listWidget->setCurrentRow(0);

    // setting
    auto settings = main->getSettings();

    // lang comboBox
    QList<langItem> langs = { {tr("System Default"), ""}, {"English", "en_US"}, {"Japanese 日本語", "ja_JP"} };
    foreach (auto lang, langs) {
        ui->cmbLanguage->addItem(lang.name, lang.locale);
        if (lang.locale == settings->value("language").toString())
            ui->cmbLanguage->setCurrentText(lang.name);
    }
    ui->cmbPingInterval->setCurrentText(settings->value("pingInterval").toString());
    ui->cmbPingWaitTime->setCurrentText(settings->value("pingWaitTime").toString());
    ui->cmbWakeTimeout->setCurrentText(settings->value("wakeTimeout").toString());
    ui->chkOpenLink->setChecked(settings->value("opneLink").toBool());

    // icon
    ui->chkDarkIcon->setChecked(settings->value("chkDarkIcon").toBool());

    // style
    auto style = settings->value("style").toString();
    ui->treeStyle->setHeaderLabels({tr("Style"), tr("Desktop environment")});
    static const QMap<QString,QString> styles = {
        {"Breeze", "KDE"},
        {"Oxygen", "KDE"},
        {"Fusion", "KDE XFce Gnome"},
        {"Windows", "KDE XFce Gnome"}
    };
    foreach (auto key, styles.keys()) {
        auto *item = new QTreeWidgetItem;
        item->setText(0, key);
        item->setText(1, styles[key]);
        ui->treeStyle->addTopLevelItem(item);
        if (style == key)
            ui->treeStyle->setCurrentItem(item);
    }
    connect(ui->treeStyle, &QTreeWidget::itemDoubleClicked, [=] (QTreeWidgetItem* item) {
        qApp->setStyle(QStyleFactory::create(item->text(0)));
    });

    // rdp link path
    ui->editPathNomachine->setText(settings->value("editPathNomachine").toString());
    ui->editPathRemmina->setText(settings->value("editPathRemmina").toString());
    ui->editPathOther->setText(settings->value("editPathOther").toString());

    // suspend
    ui->editPathSuspend->setText(settings->value("editPathSuspend").toString());

    // scanner
    ui->chkOuiCompleted->setChecked(settings->value("chkOuiCompleted").toBool());
    ui->cmbOuiUrl->addItems({ settings->defaultValues["cmbOuiUrl"].toString(), "https://linuxnet.ca/ieee/oui.txt" });
    ui->cmbOuiUrl->setCurrentText(settings->value("cmbOuiUrl").toString());
    ui->editOuiFile->setText(settings->value("editOuiFile").toString());
    connect(ui->editOuiFile, &EditPathFileSelect::actionClicked, this, [=] () {
        QFileInfo info(ui->editOuiFile->text());
        QProcess::startDetached("xdg-open",
            { info.exists()? info.absoluteFilePath() : info.absolutePath() });
    });

    // about
    ui->laAppName->setText(tr("%1 - %2").arg(qApp->applicationName(), qApp->applicationVersion()));
    ui->chkVersionCheck->setChecked(settings->value("chkVersionCheck").toBool());
    connect(ui->btnAboutQt, &QPushButton::clicked, this, [=] { qApp->aboutQt(); });

    connect(ui->cmbLanguage, &QComboBox::currentTextChanged, this, [=] { saveReboot(); });
    connect(ui->chkDarkIcon, &QCheckBox::clicked, this, [=] { saveReboot(); });
    restoreGeometry(settings->valueObj(this, "geometry").toByteArray());
}

DialogSettings::~DialogSettings()
{
    if (!resetClose) main->getSettings()->setValueObj(this, "geometry", saveGeometry());
    delete ui;
}

void DialogSettings::on_buttonBox_accepted()
{
    save();
}

void DialogSettings::save()
{
    // setting
    auto settings = main->getSettings();
    settings->setValue("language", ui->cmbLanguage->currentData());
    settings->setValue("pingInterval", ui->cmbPingInterval->currentText());
    settings->setValue("pingWaitTime", ui->cmbPingWaitTime->currentText());
    settings->setValue("wakeTimeout", ui->cmbWakeTimeout->currentText());
    settings->setValue("opneLink", ui->chkOpenLink->isChecked());

    // icon
    settings->setValue("chkDarkIcon", ui->chkDarkIcon->isChecked());

    // style
    settings->setValue("style", ui->treeStyle->selectedItems()[0]->text(0));

    // rdp link path
    settings->setValue("editPathNomachine", ui->editPathNomachine->text());
    settings->setValue("editPathRemmina", ui->editPathRemmina->text());
    settings->setValue("editPathOther", ui->editPathOther->text());

    // scanner
    if (!QFileInfo::exists(ui->editOuiFile->text()))
        ui->chkOuiCompleted->setChecked(false);

    settings->setValue("editOuiFile", ui->editOuiFile->text());
    settings->setValue("cmbOuiUrl", ui->cmbOuiUrl->currentText());
    settings->setValue("chkOuiCompleted", ui->chkOuiCompleted->isChecked());

    // help
    settings->setValue("chkVersionCheck", ui->chkVersionCheck->isChecked());
}

void DialogSettings::saveReboot()
{
    QMessageBox msgBox(this);
    msgBox.setWindowTitle(tr("Application restart"));
    msgBox.setText(tr("Do you want to restart the application?\nThis setting will be reflected after restart."));
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Cancel);
    msgBox.setIcon(QMessageBox::Question);
    if (msgBox.exec() != QMessageBox::Yes) return;

    save();
    // restart:
    qApp->quit();
    QProcess::startDetached(qApp->arguments()[0], qApp->arguments());
}

void DialogSettings::on_btnReset_clicked()
{
    QMessageBox msgBox(this);
    msgBox.setWindowTitle(tr("Reset settings"));
    msgBox.setText(tr("Are you sure you want to reset settings?\nRestart the application."));
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Cancel);
    msgBox.setIcon(QMessageBox::Question);
    if (msgBox.exec() != QMessageBox::Yes) return;

    main->getSettings()->clear();
    resetClose = true;

    // restart:
    qApp->quit();
    QProcess::startDetached(qApp->arguments()[0], qApp->arguments());
}

