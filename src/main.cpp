#include "mainwindow.h"

#include <QApplication>
#include <singleapplication.h>
#include <QFile>
#include <QTranslator>
#include <QCommandLineParser>
#include "util/settings.h"

void setStyleSheet(const QStringList &cssNames) {
    QString cssText;
    foreach (auto cssName, cssNames) {
        QFile file(QString(":/css/%1.css").arg(cssName));
        if (file.open(QFile::ReadOnly)) {
            cssText += QString::fromUtf8(file.readAll());
            file.close();
        }
    }
    qApp->setStyleSheet(cssText);
}

int main(int argc, char *argv[])
{
    SingleApplication app(argc, argv);

    QString appName = "QWakeOnLan";
    app.setApplicationName(appName);
    app.setApplicationVersion(VERSION);

    QCommandLineParser parser;
    parser.setApplicationDescription("Wake up your devices using WakenOnLan");
    parser.addHelpOption();
    parser.addVersionOption();

    QCommandLineOption optionP({"p", "profile"},
        "Manage settings and local application data individually for each profile", "Profile");
    parser.addOption(optionP);
    parser.process(QCoreApplication::arguments());

    if (parser.isSet(optionP))
        app.setApplicationName(appName + "-" + parser.value(optionP));

    Settings settings;

    // translate
    auto lang = settings.value("language").toString();
    if (lang.isEmpty()) // is System Default
        lang = QLocale::system().name();

    QTranslator translator;
    translator.load(lang, ":/i18n");
    app.installTranslator(&translator);

    // icon load
    bool chkDarkIcon = settings.value("chkDarkIcon").toBool();
    QIcon::setThemeName(chkDarkIcon? "breeze-dark" : "breeze");

    // css style
    auto cssNames = QStringList() << "common" << (chkDarkIcon? "dark" : "light");
    setStyleSheet(cssNames);

    MainWindow main;
    main.show();
    return app.exec();
}
