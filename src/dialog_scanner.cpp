#include "dialog_scanner.h"
#include "ui_dialog_scanner.h"

#include <QApplication>
#include <QDesktopWidget>
#include <QClipboard>
#include <QFutureWatcher>
#include <QProgressDialog>
#include <QtConcurrent>
#include <util/menu.h>

DialogScanner::DialogScanner(QWidget *parent) : QDialog(parent)
  , sqlModel(new SqlModel("scanner", "id"))
  , main(qobject_cast<MainWindow*>(parent))
  , ui(new Ui::DialogScanner)
{
    ui->setupUi(this);

    // dbError
    connect(sqlModel, &SqlModel::sqlError, main, &MainWindow::onSqlError);

    // action
    connect(ui->actionAdd, &QAction::triggered, [=] {
        auto id = ui->tableView->getSelectedId();
        if (!id) return;

        auto rs = sqlModel->getRecord(id);
        auto host = new Host(
                    rs.value("host").toString(),
                    rs.value("vendor").toString(),
                    rs.value("mac").toString());
        main->openEdit(parent, -1, host);
    });
    connect(ui->actionRestFilter, &QAction::triggered, ui->tableView, &TableViewSqlFilter::resetFiler);
    connect(ui->actionAutoSize, &QAction::triggered, this, [=] () {
        ui->tableView->autoSizeColRow();

        auto szTable = ui->tableView->getContentsSize();
        QSize szInner(szTable.width(), szTable.height() + ui->frameHead->height() + ui->frameFooter->height());
        QRect rcScreen = QApplication::desktop()->screenGeometry(this);
        if ((szInner.height() + pos().y()) > rcScreen.height())
            szInner.setHeight(rcScreen.height() - pos().y());
        resize(szInner);
    });

    // ip range
    auto ipRange = main->getSettings()->valueObj(this, "ipRange").toString();
    if (ipRange.isEmpty())
        ui->editIp->setAutoRange();
    else
        ui->editIp->setText(ipRange);

    // scan
    connect(ui->editIp, &EditIP::actionSearchClicked, this, &DialogScanner::scanHosts);

    // tableView
    ui->tableView->setSqlModel(sqlModel);
    ui->tableView->initSql();
    static const QStringList headers =
        { tr("ID"), tr("Host"), tr("MAC address"), tr("MAC vendor name") };
    ui->tableView->setHeaderLabels(headers);

    connect(ui->tableView, &TableViewSqlFilter::doubleClicked, [=] (const QModelIndex &/*index*/) {
        ui->actionAdd->trigger();
    });

    // setting
    restoreGeometry(main->getSettings()->valueObj(this, "geometry").toByteArray());
    auto state = main->getSettings()->valueObj(this, "columnsSave", "").toByteArray();
    if (!state.isEmpty())
        ui->tableView->restoreState(state);
}

DialogScanner::~DialogScanner()
{
    main->getSettings()->setValueObj(this, "columnsSave", ui->tableView->saveState());
    main->getSettings()->setValueObj(this, "geometry", saveGeometry());
    delete sqlModel;
    delete ui;
}

void DialogScanner::scanHosts()
{
    if (!ui->editIp->parseIP()) return;
    QElapsedTimer timer;
    timer.start();

    int ipMin = ui->editIp->getIpMin();
    int ipMax = ui->editIp->getIpMax();
    int pingWait = main->getSettings()->value("pingWaitTime").toInt();

    // Prepare Hosts to scan
    QList<Host*> hosts;
    for (int i = ipMin; i <= ipMax; i++)
        hosts << new Host(pingWait, ui->editIp->getIp(i));

    QProgressDialog progDlg(this);
    progDlg.setWindowTitle(tr("Scan"));
    progDlg.setMinimumSize(300, 150);
    progDlg.setWindowModality(Qt::WindowModal);
    progDlg.setLabelText(tr("\n%1　Scanning… threads：%2")
                         .arg(ui->editIp->text()).arg(QThread::idealThreadCount()));

    QFutureWatcher<void> futureWatcher;
    connect(&futureWatcher, &QFutureWatcherBase::progressRangeChanged, &progDlg, &QProgressDialog::setRange);
    connect(&futureWatcher, &QFutureWatcherBase::progressValueChanged, &progDlg, &QProgressDialog::setValue);
    connect(&progDlg, &QProgressDialog::canceled, &futureWatcher, &QFutureWatcherBase::cancel);

    connect(&futureWatcher, &QFutureWatcherBase::finished, &progDlg, &QProgressDialog::reset);
    connect(&futureWatcher, &QFutureWatcherBase::finished, [=] {
        if (sqlModel->getRecordCount())
            sqlModel->deleteTable("scanner");

        QSqlDatabase::database().transaction();
        foreach (auto host, hosts) {
            if (host->isActive()) {
                sqlModel->setValueClear();
                sqlModel->setValue("id");
                sqlModel->setValue("host", host->getIp());
                sqlModel->setValue("mac", host->getMac());
                sqlModel->setValue("vendor", host->getVendorName());
                sqlModel->saveValues();
            }
        }
        QSqlDatabase::database().commit();

        ui->tableView->reQuery();
        ui->actionAutoSize->trigger();
        main->getSettings()->setValueObj(this, "ipRange", ui->editIp->text());
    });

    // Multithreaded configuration
    futureWatcher.setFuture(QtConcurrent::map(hosts, [=] (Host *hostScan) {
        hostScan->scan();
    }));
    progDlg.exec();
    futureWatcher.waitForFinished();    // Waiting for thread to end

    double scanTime = static_cast<double>(timer.elapsed()) / 1000;
    ui->message->setText(tr("%1 hosts scanned in %2 seconds (%3 hosts/sec) %4 responded")
                             .arg(hosts.count())
                             .arg(scanTime, 0, 'f', 2)
                             .arg(static_cast<double>(hosts.count() / scanTime), 0, 'f', 2)
                             .arg(sqlModel->getRecordCount()));

    qDeleteAll(hosts);
    hosts.clear();
}

void DialogScanner::on_tableView_customContextMenuRequested(const QPoint &pos)
{
    Menu menu(this);

    ui->actionAdd->setEnabled(ui->tableView->isSelected());
    menu.addAction(ui->actionAdd);
    menu.addSeparator();
    menu.addAction(ui->actionRestFilter);
    menu.addAction(ui->actionAutoSize);
    menu.exec(ui->tableView->viewport()->mapToGlobal(pos));
}

void DialogScanner::on_btnClear_clicked()
{
    if (sqlModel->getRecordCount())
        sqlModel->deleteTable("scanner");
    ui->tableView->reQuery();
}

void DialogScanner::on_btnCopy_clicked()
{
    auto model = sqlModel->getModel();
    QStringList rows;
    for (int row = 0; row < model->rowCount(); row++) {
        auto rs = model->record(row);
        QStringList cols;
        for (int col = 0; col < model->columnCount(); col++)
            cols << QString("\"%1\"").arg(rs.value(col).toString());
        rows << cols.join(",");
    }
    QClipboard *clipboard = QApplication::clipboard();
    clipboard->setText(rows.join("\n"));
}
