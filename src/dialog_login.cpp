#include "dialog_login.h"
#include "ui_dialog_login.h"

#include "util/settings.h"

DialogLogin::DialogLogin(const QString &name, const QString &user, const QString &pass, QWidget *parent)
    : QDialog(parent)
    , user(user)
    , pass(pass)
    , settings(new Settings)
    , ui(new Ui::DialogLogin)
{
    ui->setupUi(this);
    ui->laName->setText(name);
    ui->editUser->setText(user);
    ui->editPass->setText(pass);
    ui->chkSave->setChecked(settings->value("loginSave").toBool());
}

DialogLogin::~DialogLogin()
{
    delete settings;
    delete ui;
}

void DialogLogin::on_buttonBox_accepted()
{
    user = ui->editUser->text();
    pass = ui->editPass->text();
    isSave = ui->chkSave->isChecked();
    settings->setValue("loginSave", isSave);
}

