#ifndef DIALOG_SETTINGS_H
#define DIALOG_SETTINGS_H

#include <QDialog>
#include "mainwindow.h"

namespace Ui {
class DialogSettings;
}

class DialogSettings : public QDialog
{
    Q_OBJECT

public:
    explicit DialogSettings(QWidget *parent = nullptr);
    ~DialogSettings();

private slots:
    void on_buttonBox_accepted();

    void on_btnReset_clicked();

private:
    void save();
    void saveReboot();
    bool resetClose = false;
    struct menuItem {
        QString name;
        QString icon;
    };
    struct langItem {
        QString name;
        QString locale;
    };

    MainWindow* main;
    Ui::DialogSettings *ui;
};

#endif // DIALOG_SETTINGS_H
