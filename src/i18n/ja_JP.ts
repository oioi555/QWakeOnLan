<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ja_JP" sourcelanguage="en">
<context>
    <name>BaseMainWindow</name>
    <message>
        <location filename="../util/base_mainwindow.cpp" line="14"/>
        <source>Icons and text (vertical)</source>
        <translation>アイコンとテキスト（縦）</translation>
    </message>
    <message>
        <location filename="../util/base_mainwindow.cpp" line="15"/>
        <source>Icons and text (horizontal)</source>
        <translation>アイコンとテキスト（横）</translation>
    </message>
    <message>
        <location filename="../util/base_mainwindow.cpp" line="16"/>
        <source>Icon only</source>
        <translation>アイコンのみ</translation>
    </message>
    <message>
        <location filename="../util/base_mainwindow.cpp" line="17"/>
        <source>Text only</source>
        <translation>テキストのみ</translation>
    </message>
    <message>
        <location filename="../util/base_mainwindow.cpp" line="78"/>
        <source>Toolbar style</source>
        <translation>ツールバースタイル</translation>
    </message>
    <message>
        <location filename="../util/base_mainwindow.cpp" line="90"/>
        <source>Delete</source>
        <translation>削除</translation>
    </message>
    <message>
        <location filename="../util/base_mainwindow.cpp" line="91"/>
        <source>Are you sure you want to delete it?</source>
        <translation>本当に削除しますか？</translation>
    </message>
</context>
<context>
    <name>DialogEdit</name>
    <message>
        <location filename="../dialog_edit.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialog_edit.ui" line="28"/>
        <source>Basic</source>
        <translation>基本</translation>
    </message>
    <message>
        <location filename="../dialog_edit.ui" line="104"/>
        <source>Host :</source>
        <translation>ホスト :</translation>
    </message>
    <message>
        <location filename="../dialog_edit.ui" line="137"/>
        <source>HH:HH:HH:HH:HH:HH;_</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialog_edit.ui" line="148"/>
        <source>RDP link</source>
        <translation>RDPリンク</translation>
    </message>
    <message>
        <location filename="../dialog_edit.ui" line="184"/>
        <source>Suspend</source>
        <translation>サスペンド</translation>
    </message>
    <message>
        <location filename="../dialog_edit.ui" line="190"/>
        <source>Suspend script</source>
        <translation>サスペンド スクリプト</translation>
    </message>
    <message>
        <location filename="../dialog_edit.ui" line="205"/>
        <source>Script argument</source>
        <translation>スクリプト引数</translation>
    </message>
    <message>
        <location filename="../dialog_edit.ui" line="242"/>
        <source>The default port is empty.</source>
        <translation>空欄でデフォルトポート。</translation>
    </message>
    <message>
        <location filename="../dialog_edit.ui" line="249"/>
        <source>SSH port :</source>
        <translation>SSHポート :</translation>
    </message>
    <message>
        <location filename="../dialog_edit.ui" line="211"/>
        <source>User name :</source>
        <translation>ユーザー名 :</translation>
    </message>
    <message>
        <location filename="../dialog_edit.ui" line="225"/>
        <source>Password :</source>
        <translation>パスワード :</translation>
    </message>
    <message>
        <location filename="../dialog_edit.ui" line="74"/>
        <source>ID :</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialog_edit.ui" line="118"/>
        <source>MAC address :</source>
        <translation>MACアドレス :</translation>
    </message>
    <message>
        <location filename="../dialog_edit.ui" line="94"/>
        <source>Device name :</source>
        <translation>デバイス名 :</translation>
    </message>
    <message>
        <location filename="../dialog_edit.cpp" line="26"/>
        <source>Device：%1</source>
        <translation>デバイス：%1</translation>
    </message>
    <message>
        <location filename="../dialog_edit.cpp" line="26"/>
        <source>Add</source>
        <translation>追加</translation>
    </message>
    <message>
        <location filename="../dialog_edit.cpp" line="26"/>
        <source>Edit</source>
        <translation>編集</translation>
    </message>
    <message>
        <location filename="../dialog_edit.cpp" line="57"/>
        <source>add new</source>
        <translation>新規登録</translation>
    </message>
    <message>
        <location filename="../dialog_edit.cpp" line="118"/>
        <location filename="../dialog_edit.cpp" line="123"/>
        <location filename="../dialog_edit.cpp" line="128"/>
        <source>validation</source>
        <translation>入力確認</translation>
    </message>
    <message>
        <location filename="../dialog_edit.cpp" line="118"/>
        <source>Missning device name</source>
        <translation>デバイス名を入力してください</translation>
    </message>
    <message>
        <location filename="../dialog_edit.cpp" line="123"/>
        <source>Missning Host</source>
        <translation>ホストを入力してください</translation>
    </message>
    <message>
        <location filename="../dialog_edit.cpp" line="128"/>
        <source>Missning RDP File</source>
        <translation>RDPファイルを入力してください</translation>
    </message>
    <message>
        <location filename="../dialog_edit.cpp" line="151"/>
        <source>DB Error: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialog_edit.cpp" line="152"/>
        <source>DB Error</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>DialogLogin</name>
    <message>
        <location filename="../dialog_login.ui" line="14"/>
        <source>Login</source>
        <translation>ログイン</translation>
    </message>
    <message>
        <location filename="../dialog_login.ui" line="42"/>
        <source>Device Name:</source>
        <translation>デバイス名 :</translation>
    </message>
    <message>
        <location filename="../dialog_login.ui" line="22"/>
        <source>User name :</source>
        <translation>ユーザー名 :</translation>
    </message>
    <message>
        <location filename="../dialog_login.ui" line="32"/>
        <source>Password :</source>
        <translation>パスワード :</translation>
    </message>
    <message>
        <location filename="../dialog_login.ui" line="49"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialog_login.ui" line="61"/>
        <source>Save login information</source>
        <translation>ログイン情報を保存する</translation>
    </message>
</context>
<context>
    <name>DialogScanner</name>
    <message>
        <location filename="../dialog_scanner.ui" line="14"/>
        <source>Network scanner</source>
        <translation>ネットワーク・スキャナー</translation>
    </message>
    <message>
        <location filename="../dialog_scanner.ui" line="66"/>
        <source>IP address range</source>
        <translation>IPアドレス範囲</translation>
    </message>
    <message>
        <location filename="../dialog_scanner.ui" line="104"/>
        <source>Copy (CSV)</source>
        <translation>コピー(CSV)</translation>
    </message>
    <message>
        <location filename="../dialog_scanner.ui" line="107"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialog_scanner.ui" line="124"/>
        <source>Delete</source>
        <translation>削除</translation>
    </message>
    <message>
        <location filename="../dialog_scanner.ui" line="163"/>
        <source>Status</source>
        <translation>ステータス</translation>
    </message>
    <message>
        <location filename="../dialog_scanner.ui" line="200"/>
        <source>Auto Size</source>
        <translation>自動サイズ</translation>
    </message>
    <message>
        <location filename="../dialog_scanner.ui" line="203"/>
        <source>Automatically adjust the column width</source>
        <translation>自動的に列幅を調整します</translation>
    </message>
    <message>
        <location filename="../dialog_scanner.ui" line="206"/>
        <source>W</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialog_scanner.ui" line="239"/>
        <source>A</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialog_scanner.ui" line="218"/>
        <source>Filter reset</source>
        <translation>フィルタリセット</translation>
    </message>
    <message>
        <location filename="../dialog_scanner.ui" line="221"/>
        <source>Reset the filter</source>
        <translation>フィルターをリセットします</translation>
    </message>
    <message>
        <location filename="../dialog_scanner.ui" line="224"/>
        <source>R</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialog_scanner.ui" line="236"/>
        <source>Add device</source>
        <translation>デバイスの追加</translation>
    </message>
    <message>
        <location filename="../dialog_scanner.cpp" line="60"/>
        <source>ID</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialog_scanner.cpp" line="60"/>
        <source>MAC address</source>
        <translation>MACアドレス</translation>
    </message>
    <message>
        <location filename="../dialog_scanner.cpp" line="60"/>
        <source>MAC vendor name</source>
        <translation>MACベンダー名</translation>
    </message>
    <message>
        <location filename="../dialog_scanner.cpp" line="98"/>
        <source>Scan</source>
        <translation>スキャン</translation>
    </message>
    <message>
        <location filename="../dialog_scanner.cpp" line="140"/>
        <source>%1 hosts scanned in %2 seconds (%3 hosts/sec) %4 responded</source>
        <translation>%1 台のホストを %2 秒 (%3 [host/sec])でスキャンしました。%4 台が応答</translation>
    </message>
    <message>
        <location filename="../dialog_scanner.cpp" line="60"/>
        <source>Host</source>
        <translation>ホスト</translation>
    </message>
    <message>
        <location filename="../dialog_scanner.cpp" line="101"/>
        <source>
%1&#x3000;Scanning… threads：%2</source>
        <translation>
%1&#x3000;スキャン中… スレッド：%2</translation>
    </message>
</context>
<context>
    <name>DialogSettings</name>
    <message>
        <location filename="../dialog_settings.ui" line="14"/>
        <source>Settings</source>
        <translation>設定</translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="60"/>
        <source>Language</source>
        <translation>言語</translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="79"/>
        <source>Icon</source>
        <translation>アイコン</translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="85"/>
        <source>Enable if the system theme is dark style</source>
        <translation>システムテーマがダークスタイルの場合に有効にする</translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="88"/>
        <source>Dark Theme Icons</source>
        <translation>ダークテーマ用アイコンを使用</translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="108"/>
        <source>Qt Style</source>
        <translation>Qtスタイル</translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="130"/>
        <location filename="../dialog_settings.cpp" line="21"/>
        <source>Ping</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="136"/>
        <source>Interval : </source>
        <translation>インターバル : </translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="144"/>
        <location filename="../dialog_settings.ui" line="290"/>
        <source>30</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="149"/>
        <source>50</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="154"/>
        <source>80</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="159"/>
        <source>100</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="168"/>
        <source>500</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="173"/>
        <source>1000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="178"/>
        <source>1500</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="183"/>
        <source>2000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="204"/>
        <location filename="../dialog_settings.ui" line="211"/>
        <source>msec</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="218"/>
        <location filename="../dialog_settings.ui" line="267"/>
        <source>Response waiting : </source>
        <translation>応答待ち : </translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="245"/>
        <source>Operation at Turn on</source>
        <translation>起動時の操作</translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="251"/>
        <source>Automatically open RDP links</source>
        <translation>RDPリンクを自動的に開く</translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="261"/>
        <source>Response</source>
        <translation>応答</translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="377"/>
        <source>Script path</source>
        <translation>スクリプトパス</translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="422"/>
        <source> MAC vendor list (oui.txt)</source>
        <translation> MACベンダーリスト (oui.txt)</translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="428"/>
        <source>oui.txt file :</source>
        <translation>oui.txt ファイル :</translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="435"/>
        <source>Download url :</source>
        <translation>ダウンロード url :</translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="461"/>
        <source>If off, re-download</source>
        <translation>オフなら再ダウンロード</translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="467"/>
        <source>Download completed</source>
        <translation>ダウンロードが完了しました</translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="579"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Noto Mono&apos;;&quot;&gt;Copyright&lt;/span&gt;© &lt;span style=&quot; font-family:&apos;Noto Mono&apos;;&quot;&gt;2020 oioi&lt;/span&gt;&lt;br/&gt;&lt;a href=&quot;https://gitlab.com/oioi555/QWakeOnLan&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#008098;&quot;&gt;https://gitlab.com/oioi555/QWakeOnLan&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="602"/>
        <source>Version check</source>
        <translation>バージョンチェック</translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="608"/>
        <source>Check the Latest version at startup</source>
        <translation>起動時に最新バージョンを確認する</translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="615"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;https://gitlab.com/oioi555/QWakeOnLan/-/releases&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#008098;&quot;&gt;Release page&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="715"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;                    GNU GENERAL PUBLIC LICENSE&lt;/p&gt;&lt;p&gt;                       Version 2, June 1991&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt; Copyright (C) 1989, 1991 Free Software Foundation, Inc.,&lt;/p&gt;&lt;p&gt; 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA&lt;/p&gt;&lt;p&gt; Everyone is permitted to copy and distribute verbatim copies&lt;/p&gt;&lt;p&gt; of this license document, but changing it is not allowed.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;                            Preamble&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;  The licenses for most software are designed to take away your&lt;/p&gt;&lt;p&gt;freedom to share and change it.  By contrast, the GNU General Public&lt;/p&gt;&lt;p&gt;License is intended to guarantee your freedom to share and change free&lt;/p&gt;&lt;p&gt;software--to make sure the software is free for all its users.  This&lt;/p&gt;&lt;p&gt;General Public License applies to most of the Free Software&lt;/p&gt;&lt;p&gt;Foundation&apos;s software and to any other program whose authors commit to&lt;/p&gt;&lt;p&gt;using it.  (Some other Free Software Foundation software is covered by&lt;/p&gt;&lt;p&gt;the GNU Lesser General Public License instead.)  You can apply it to&lt;/p&gt;&lt;p&gt;your programs, too.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;  When we speak of free software, we are referring to freedom, not&lt;/p&gt;&lt;p&gt;price.  Our General Public Licenses are designed to make sure that you&lt;/p&gt;&lt;p&gt;have the freedom to distribute copies of free software (and charge for&lt;/p&gt;&lt;p&gt;this service if you wish), that you receive source code or can get it&lt;/p&gt;&lt;p&gt;if you want it, that you can change the software or use pieces of it&lt;/p&gt;&lt;p&gt;in new free programs; and that you know you can do these things.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;  To protect your rights, we need to make restrictions that forbid&lt;/p&gt;&lt;p&gt;anyone to deny you these rights or to ask you to surrender the rights.&lt;/p&gt;&lt;p&gt;These restrictions translate to certain responsibilities for you if you&lt;/p&gt;&lt;p&gt;distribute copies of the software, or if you modify it.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;  For example, if you distribute copies of such a program, whether&lt;/p&gt;&lt;p&gt;gratis or for a fee, you must give the recipients all the rights that&lt;/p&gt;&lt;p&gt;you have.  You must make sure that they, too, receive or can get the&lt;/p&gt;&lt;p&gt;source code.  And you must show them these terms so they know their&lt;/p&gt;&lt;p&gt;rights.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;  We protect your rights with two steps: (1) copyright the software, and&lt;/p&gt;&lt;p&gt;(2) offer you this license which gives you legal permission to copy,&lt;/p&gt;&lt;p&gt;distribute and/or modify the software.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;  Also, for each author&apos;s protection and ours, we want to make certain&lt;/p&gt;&lt;p&gt;that everyone understands that there is no warranty for this free&lt;/p&gt;&lt;p&gt;software.  If the software is modified by someone else and passed on, we&lt;/p&gt;&lt;p&gt;want its recipients to know that what they have is not the original, so&lt;/p&gt;&lt;p&gt;that any problems introduced by others will not reflect on the original&lt;/p&gt;&lt;p&gt;authors&apos; reputations.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;  Finally, any free program is threatened constantly by software&lt;/p&gt;&lt;p&gt;patents.  We wish to avoid the danger that redistributors of a free&lt;/p&gt;&lt;p&gt;program will individually obtain patent licenses, in effect making the&lt;/p&gt;&lt;p&gt;program proprietary.  To prevent this, we have made it clear that any&lt;/p&gt;&lt;p&gt;patent must be licensed for everyone&apos;s free use or not licensed at all.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;  The precise terms and conditions for copying, distribution and&lt;/p&gt;&lt;p&gt;modification follow.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;                    GNU GENERAL PUBLIC LICENSE&lt;/p&gt;&lt;p&gt;   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;  0. This License applies to any program or other work which contains&lt;/p&gt;&lt;p&gt;a notice placed by the copyright holder saying it may be distributed&lt;/p&gt;&lt;p&gt;under the terms of this General Public License.  The &amp;quot;Program&amp;quot;, below,&lt;/p&gt;&lt;p&gt;refers to any such program or work, and a &amp;quot;work based on the Program&amp;quot;&lt;/p&gt;&lt;p&gt;means either the Program or any derivative work under copyright law:&lt;/p&gt;&lt;p&gt;that is to say, a work containing the Program or a portion of it,&lt;/p&gt;&lt;p&gt;either verbatim or with modifications and/or translated into another&lt;/p&gt;&lt;p&gt;language.  (Hereinafter, translation is included without limitation in&lt;/p&gt;&lt;p&gt;the term &amp;quot;modification&amp;quot;.)  Each licensee is addressed as &amp;quot;you&amp;quot;.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;Activities other than copying, distribution and modification are not&lt;/p&gt;&lt;p&gt;covered by this License; they are outside its scope.  The act of&lt;/p&gt;&lt;p&gt;running the Program is not restricted, and the output from the Program&lt;/p&gt;&lt;p&gt;is covered only if its contents constitute a work based on the&lt;/p&gt;&lt;p&gt;Program (independent of having been made by running the Program).&lt;/p&gt;&lt;p&gt;Whether that is true depends on what the Program does.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;  1. You may copy and distribute verbatim copies of the Program&apos;s&lt;/p&gt;&lt;p&gt;source code as you receive it, in any medium, provided that you&lt;/p&gt;&lt;p&gt;conspicuously and appropriately publish on each copy an appropriate&lt;/p&gt;&lt;p&gt;copyright notice and disclaimer of warranty; keep intact all the&lt;/p&gt;&lt;p&gt;notices that refer to this License and to the absence of any warranty;&lt;/p&gt;&lt;p&gt;and give any other recipients of the Program a copy of this License&lt;/p&gt;&lt;p&gt;along with the Program.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;You may charge a fee for the physical act of transferring a copy, and&lt;/p&gt;&lt;p&gt;you may at your option offer warranty protection in exchange for a fee.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;  2. You may modify your copy or copies of the Program or any portion&lt;/p&gt;&lt;p&gt;of it, thus forming a work based on the Program, and copy and&lt;/p&gt;&lt;p&gt;distribute such modifications or work under the terms of Section 1&lt;/p&gt;&lt;p&gt;above, provided that you also meet all of these conditions:&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;    a) You must cause the modified files to carry prominent notices&lt;/p&gt;&lt;p&gt;    stating that you changed the files and the date of any change.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;    b) You must cause any work that you distribute or publish, that in&lt;/p&gt;&lt;p&gt;    whole or in part contains or is derived from the Program or any&lt;/p&gt;&lt;p&gt;    part thereof, to be licensed as a whole at no charge to all third&lt;/p&gt;&lt;p&gt;    parties under the terms of this License.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;    c) If the modified program normally reads commands interactively&lt;/p&gt;&lt;p&gt;    when run, you must cause it, when started running for such&lt;/p&gt;&lt;p&gt;    interactive use in the most ordinary way, to print or display an&lt;/p&gt;&lt;p&gt;    announcement including an appropriate copyright notice and a&lt;/p&gt;&lt;p&gt;    notice that there is no warranty (or else, saying that you provide&lt;/p&gt;&lt;p&gt;    a warranty) and that users may redistribute the program under&lt;/p&gt;&lt;p&gt;    these conditions, and telling the user how to view a copy of this&lt;/p&gt;&lt;p&gt;    License.  (Exception: if the Program itself is interactive but&lt;/p&gt;&lt;p&gt;    does not normally print such an announcement, your work based on&lt;/p&gt;&lt;p&gt;    the Program is not required to print an announcement.)&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;These requirements apply to the modified work as a whole.  If&lt;/p&gt;&lt;p&gt;identifiable sections of that work are not derived from the Program,&lt;/p&gt;&lt;p&gt;and can be reasonably considered independent and separate works in&lt;/p&gt;&lt;p&gt;themselves, then this License, and its terms, do not apply to those&lt;/p&gt;&lt;p&gt;sections when you distribute them as separate works.  But when you&lt;/p&gt;&lt;p&gt;distribute the same sections as part of a whole which is a work based&lt;/p&gt;&lt;p&gt;on the Program, the distribution of the whole must be on the terms of&lt;/p&gt;&lt;p&gt;this License, whose permissions for other licensees extend to the&lt;/p&gt;&lt;p&gt;entire whole, and thus to each and every part regardless of who wrote it.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;Thus, it is not the intent of this section to claim rights or contest&lt;/p&gt;&lt;p&gt;your rights to work written entirely by you; rather, the intent is to&lt;/p&gt;&lt;p&gt;exercise the right to control the distribution of derivative or&lt;/p&gt;&lt;p&gt;collective works based on the Program.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;In addition, mere aggregation of another work not based on the Program&lt;/p&gt;&lt;p&gt;with the Program (or with a work based on the Program) on a volume of&lt;/p&gt;&lt;p&gt;a storage or distribution medium does not bring the other work under&lt;/p&gt;&lt;p&gt;the scope of this License.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;  3. You may copy and distribute the Program (or a work based on it,&lt;/p&gt;&lt;p&gt;under Section 2) in object code or executable form under the terms of&lt;/p&gt;&lt;p&gt;Sections 1 and 2 above provided that you also do one of the following:&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;    a) Accompany it with the complete corresponding machine-readable&lt;/p&gt;&lt;p&gt;    source code, which must be distributed under the terms of Sections&lt;/p&gt;&lt;p&gt;    1 and 2 above on a medium customarily used for software interchange; or,&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;    b) Accompany it with a written offer, valid for at least three&lt;/p&gt;&lt;p&gt;    years, to give any third party, for a charge no more than your&lt;/p&gt;&lt;p&gt;    cost of physically performing source distribution, a complete&lt;/p&gt;&lt;p&gt;    machine-readable copy of the corresponding source code, to be&lt;/p&gt;&lt;p&gt;    distributed under the terms of Sections 1 and 2 above on a medium&lt;/p&gt;&lt;p&gt;    customarily used for software interchange; or,&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;    c) Accompany it with the information you received as to the offer&lt;/p&gt;&lt;p&gt;    to distribute corresponding source code.  (This alternative is&lt;/p&gt;&lt;p&gt;    allowed only for noncommercial distribution and only if you&lt;/p&gt;&lt;p&gt;    received the program in object code or executable form with such&lt;/p&gt;&lt;p&gt;    an offer, in accord with Subsection b above.)&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;The source code for a work means the preferred form of the work for&lt;/p&gt;&lt;p&gt;making modifications to it.  For an executable work, complete source&lt;/p&gt;&lt;p&gt;code means all the source code for all modules it contains, plus any&lt;/p&gt;&lt;p&gt;associated interface definition files, plus the scripts used to&lt;/p&gt;&lt;p&gt;control compilation and installation of the executable.  However, as a&lt;/p&gt;&lt;p&gt;special exception, the source code distributed need not include&lt;/p&gt;&lt;p&gt;anything that is normally distributed (in either source or binary&lt;/p&gt;&lt;p&gt;form) with the major components (compiler, kernel, and so on) of the&lt;/p&gt;&lt;p&gt;operating system on which the executable runs, unless that component&lt;/p&gt;&lt;p&gt;itself accompanies the executable.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;If distribution of executable or object code is made by offering&lt;/p&gt;&lt;p&gt;access to copy from a designated place, then offering equivalent&lt;/p&gt;&lt;p&gt;access to copy the source code from the same place counts as&lt;/p&gt;&lt;p&gt;distribution of the source code, even though third parties are not&lt;/p&gt;&lt;p&gt;compelled to copy the source along with the object code.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;  4. You may not copy, modify, sublicense, or distribute the Program&lt;/p&gt;&lt;p&gt;except as expressly provided under this License.  Any attempt&lt;/p&gt;&lt;p&gt;otherwise to copy, modify, sublicense or distribute the Program is&lt;/p&gt;&lt;p&gt;void, and will automatically terminate your rights under this License.&lt;/p&gt;&lt;p&gt;However, parties who have received copies, or rights, from you under&lt;/p&gt;&lt;p&gt;this License will not have their licenses terminated so long as such&lt;/p&gt;&lt;p&gt;parties remain in full compliance.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;  5. You are not required to accept this License, since you have not&lt;/p&gt;&lt;p&gt;signed it.  However, nothing else grants you permission to modify or&lt;/p&gt;&lt;p&gt;distribute the Program or its derivative works.  These actions are&lt;/p&gt;&lt;p&gt;prohibited by law if you do not accept this License.  Therefore, by&lt;/p&gt;&lt;p&gt;modifying or distributing the Program (or any work based on the&lt;/p&gt;&lt;p&gt;Program), you indicate your acceptance of this License to do so, and&lt;/p&gt;&lt;p&gt;all its terms and conditions for copying, distributing or modifying&lt;/p&gt;&lt;p&gt;the Program or works based on it.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;  6. Each time you redistribute the Program (or any work based on the&lt;/p&gt;&lt;p&gt;Program), the recipient automatically receives a license from the&lt;/p&gt;&lt;p&gt;original licensor to copy, distribute or modify the Program subject to&lt;/p&gt;&lt;p&gt;these terms and conditions.  You may not impose any further&lt;/p&gt;&lt;p&gt;restrictions on the recipients&apos; exercise of the rights granted herein.&lt;/p&gt;&lt;p&gt;You are not responsible for enforcing compliance by third parties to&lt;/p&gt;&lt;p&gt;this License.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;  7. If, as a consequence of a court judgment or allegation of patent&lt;/p&gt;&lt;p&gt;infringement or for any other reason (not limited to patent issues),&lt;/p&gt;&lt;p&gt;conditions are imposed on you (whether by court order, agreement or&lt;/p&gt;&lt;p&gt;otherwise) that contradict the conditions of this License, they do not&lt;/p&gt;&lt;p&gt;excuse you from the conditions of this License.  If you cannot&lt;/p&gt;&lt;p&gt;distribute so as to satisfy simultaneously your obligations under this&lt;/p&gt;&lt;p&gt;License and any other pertinent obligations, then as a consequence you&lt;/p&gt;&lt;p&gt;may not distribute the Program at all.  For example, if a patent&lt;/p&gt;&lt;p&gt;license would not permit royalty-free redistribution of the Program by&lt;/p&gt;&lt;p&gt;all those who receive copies directly or indirectly through you, then&lt;/p&gt;&lt;p&gt;the only way you could satisfy both it and this License would be to&lt;/p&gt;&lt;p&gt;refrain entirely from distribution of the Program.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;If any portion of this section is held invalid or unenforceable under&lt;/p&gt;&lt;p&gt;any particular circumstance, the balance of the section is intended to&lt;/p&gt;&lt;p&gt;apply and the section as a whole is intended to apply in other&lt;/p&gt;&lt;p&gt;circumstances.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;It is not the purpose of this section to induce you to infringe any&lt;/p&gt;&lt;p&gt;patents or other property right claims or to contest validity of any&lt;/p&gt;&lt;p&gt;such claims; this section has the sole purpose of protecting the&lt;/p&gt;&lt;p&gt;integrity of the free software distribution system, which is&lt;/p&gt;&lt;p&gt;implemented by public license practices.  Many people have made&lt;/p&gt;&lt;p&gt;generous contributions to the wide range of software distributed&lt;/p&gt;&lt;p&gt;through that system in reliance on consistent application of that&lt;/p&gt;&lt;p&gt;system; it is up to the author/donor to decide if he or she is willing&lt;/p&gt;&lt;p&gt;to distribute software through any other system and a licensee cannot&lt;/p&gt;&lt;p&gt;impose that choice.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;This section is intended to make thoroughly clear what is believed to&lt;/p&gt;&lt;p&gt;be a consequence of the rest of this License.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;  8. If the distribution and/or use of the Program is restricted in&lt;/p&gt;&lt;p&gt;certain countries either by patents or by copyrighted interfaces, the&lt;/p&gt;&lt;p&gt;original copyright holder who places the Program under this License&lt;/p&gt;&lt;p&gt;may add an explicit geographical distribution limitation excluding&lt;/p&gt;&lt;p&gt;those countries, so that distribution is permitted only in or among&lt;/p&gt;&lt;p&gt;countries not thus excluded.  In such case, this License incorporates&lt;/p&gt;&lt;p&gt;the limitation as if written in the body of this License.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;  9. The Free Software Foundation may publish revised and/or new versions&lt;/p&gt;&lt;p&gt;of the General Public License from time to time.  Such new versions will&lt;/p&gt;&lt;p&gt;be similar in spirit to the present version, but may differ in detail to&lt;/p&gt;&lt;p&gt;address new problems or concerns.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;Each version is given a distinguishing version number.  If the Program&lt;/p&gt;&lt;p&gt;specifies a version number of this License which applies to it and &amp;quot;any&lt;/p&gt;&lt;p&gt;later version&amp;quot;, you have the option of following the terms and conditions&lt;/p&gt;&lt;p&gt;either of that version or of any later version published by the Free&lt;/p&gt;&lt;p&gt;Software Foundation.  If the Program does not specify a version number of&lt;/p&gt;&lt;p&gt;this License, you may choose any version ever published by the Free Software&lt;/p&gt;&lt;p&gt;Foundation.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;  10. If you wish to incorporate parts of the Program into other free&lt;/p&gt;&lt;p&gt;programs whose distribution conditions are different, write to the author&lt;/p&gt;&lt;p&gt;to ask for permission.  For software which is copyrighted by the Free&lt;/p&gt;&lt;p&gt;Software Foundation, write to the Free Software Foundation; we sometimes&lt;/p&gt;&lt;p&gt;make exceptions for this.  Our decision will be guided by the two goals&lt;/p&gt;&lt;p&gt;of preserving the free status of all derivatives of our free software and&lt;/p&gt;&lt;p&gt;of promoting the sharing and reuse of software generally.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;                            NO WARRANTY&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY&lt;/p&gt;&lt;p&gt;FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN&lt;/p&gt;&lt;p&gt;OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES&lt;/p&gt;&lt;p&gt;PROVIDE THE PROGRAM &amp;quot;AS IS&amp;quot; WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED&lt;/p&gt;&lt;p&gt;OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF&lt;/p&gt;&lt;p&gt;MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS&lt;/p&gt;&lt;p&gt;TO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE&lt;/p&gt;&lt;p&gt;PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,&lt;/p&gt;&lt;p&gt;REPAIR OR CORRECTION.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING&lt;/p&gt;&lt;p&gt;WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR&lt;/p&gt;&lt;p&gt;REDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,&lt;/p&gt;&lt;p&gt;INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING&lt;/p&gt;&lt;p&gt;OUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED&lt;/p&gt;&lt;p&gt;TO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY&lt;/p&gt;&lt;p&gt;YOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER&lt;/p&gt;&lt;p&gt;PROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE&lt;/p&gt;&lt;p&gt;POSSIBILITY OF SUCH DAMAGES.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;                     END OF TERMS AND CONDITIONS&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;            How to Apply These Terms to Your New Programs&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;  If you develop a new program, and you want it to be of the greatest&lt;/p&gt;&lt;p&gt;possible use to the public, the best way to achieve this is to make it&lt;/p&gt;&lt;p&gt;free software which everyone can redistribute and change under these terms.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;  To do so, attach the following notices to the program.  It is safest&lt;/p&gt;&lt;p&gt;to attach them to the start of each source file to most effectively&lt;/p&gt;&lt;p&gt;convey the exclusion of warranty; and each file should have at least&lt;/p&gt;&lt;p&gt;the &amp;quot;copyright&amp;quot; line and a pointer to where the full notice is found.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;    QWakeOnLan&lt;/p&gt;&lt;p&gt;    Copyright (C) 2021  oioi&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;    This program is free software; you can redistribute it and/or modify&lt;/p&gt;&lt;p&gt;    it under the terms of the GNU General Public License as published by&lt;/p&gt;&lt;p&gt;    the Free Software Foundation; either version 2 of the License, or&lt;/p&gt;&lt;p&gt;    (at your option) any later version.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;    This program is distributed in the hope that it will be useful,&lt;/p&gt;&lt;p&gt;    but WITHOUT ANY WARRANTY; without even the implied warranty of&lt;/p&gt;&lt;p&gt;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the&lt;/p&gt;&lt;p&gt;    GNU General Public License for more details.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;    You should have received a copy of the GNU General Public License along&lt;/p&gt;&lt;p&gt;    with this program; if not, write to the Free Software Foundation, Inc.,&lt;/p&gt;&lt;p&gt;    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;Also add information on how to contact you by electronic and paper mail.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;If the program is interactive, make it output a short notice like this&lt;/p&gt;&lt;p&gt;when it starts in an interactive mode:&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;    Gnomovision version 69, Copyright (C) year name of author&lt;/p&gt;&lt;p&gt;    Gnomovision comes with ABSOLUTELY NO WARRANTY; for details type `show w&apos;.&lt;/p&gt;&lt;p&gt;    This is free software, and you are welcome to redistribute it&lt;/p&gt;&lt;p&gt;    under certain conditions; type `show c&apos; for details.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;The hypothetical commands `show w&apos; and `show c&apos; should show the appropriate&lt;/p&gt;&lt;p&gt;parts of the General Public License.  Of course, the commands you use may&lt;/p&gt;&lt;p&gt;be called something other than `show w&apos; and `show c&apos;; they could even be&lt;/p&gt;&lt;p&gt;mouse-clicks or menu items--whatever suits your program.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;You should also get your employer (if you work as a programmer) or your&lt;/p&gt;&lt;p&gt;school, if any, to sign a &amp;quot;copyright disclaimer&amp;quot; for the program, if&lt;/p&gt;&lt;p&gt;necessary.  Here is a sample; alter the names:&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;  Yoyodyne, Inc., hereby disclaims all copyright interest in the program&lt;/p&gt;&lt;p&gt;  `Gnomovision&apos; (which makes passes at compilers) written by James Hacker.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;  &amp;lt;signature of Ty Coon&amp;gt;, 1 April 1989&lt;/p&gt;&lt;p&gt;  Ty Coon, President of Vice&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;This General Public License does not permit incorporating your program into&lt;/p&gt;&lt;p&gt;proprietary programs.  If your program is a subroutine library, you may&lt;/p&gt;&lt;p&gt;consider it more useful to permit linking proprietary applications with the&lt;/p&gt;&lt;p&gt;library.  If this is what you want to do, use the GNU Lesser General&lt;/p&gt;&lt;p&gt;Public License instead of this License.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="740"/>
        <location filename="../dialog_settings.cpp" line="168"/>
        <source>Reset settings</source>
        <translation>設定をリセット</translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="275"/>
        <source>15</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="69"/>
        <location filename="../dialog_settings.ui" line="95"/>
        <source>(Needs reboot)</source>
        <translation>(再起動が必要)</translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="280"/>
        <source>20</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="285"/>
        <source>25</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="298"/>
        <source>sec</source>
        <translation>秒</translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="321"/>
        <source>RDP file folder path</source>
        <translation>RDPファイルのフォルダパス</translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="337"/>
        <source>NoMachine : </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="347"/>
        <source>Remmina : </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="327"/>
        <source>Other : </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="507"/>
        <source>About</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="637"/>
        <source>QWakeOnLan is a Qt application</source>
        <translation>QWakeOnLanはQtアプリケーションです</translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="644"/>
        <source>About Qt</source>
        <translation>Qtについて</translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="655"/>
        <source>Credits</source>
        <translation>クレジット</translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="676"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;h2 style=&quot; margin-top:16px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:x-large; font-weight:600;&quot;&gt;Library&lt;/span&gt;&lt;/h2&gt;&lt;p&gt;Breeze Icons &amp;lt;LGPLv2.1/LGPLv3&amp;gt;&lt;br/&gt;&lt;a href=&quot;https://invent.kde.org/frameworks/breeze-icons&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#4c6b8a;&quot;&gt;https://invent.kde.org/frameworks/breeze-icons&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;FilterTable &amp;lt;MIT Licence&amp;gt;&lt;br/&gt;&lt;a href=&quot;https://github.com/midzer/FilterTable&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#4c6b8a;&quot;&gt;https://github.com/midzer/FilterTable&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;SingleApplication &amp;lt;MIT Licence&amp;gt;&lt;br/&gt;&lt;a href=&quot;https://github.com/itay-grudev/SingleApplication&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;https://github.com/itay-grudev/SingleApplication&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;QBlowfish &amp;lt;MIT Licence&amp;gt;&lt;br/&gt;&lt;a href=&quot;https://github.com/itay-grudev/SingleApplication&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;https://github.com/roop/qblowfish&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;h2 style=&quot; margin-top:16px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:x-large; font-weight:600;&quot;&gt;Remote desktop client&lt;/span&gt;&lt;/h2&gt;&lt;p&gt;Remmina&lt;br/&gt;&lt;a href=&quot;https://remmina.org/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#4c6b8a;&quot;&gt;https://remmina.org/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;p&gt;NoMachine&lt;br/&gt;&lt;a href=&quot;https://www.nomachine.com/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#4c6b8a;&quot;&gt;https://www.nomachine.com/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialog_settings.ui" line="694"/>
        <source>License</source>
        <translation>ライセンス</translation>
    </message>
    <message>
        <location filename="../dialog_settings.cpp" line="16"/>
        <source>%1 : Settings</source>
        <translation>%1 : 設定</translation>
    </message>
    <message>
        <location filename="../dialog_settings.cpp" line="20"/>
        <source>General</source>
        <translation>全般</translation>
    </message>
    <message>
        <location filename="../dialog_settings.cpp" line="22"/>
        <source>RDP link</source>
        <translation>RDPリンク</translation>
    </message>
    <message>
        <location filename="../dialog_settings.cpp" line="23"/>
        <source>Suspend</source>
        <translation>サスペンド</translation>
    </message>
    <message>
        <location filename="../dialog_settings.cpp" line="24"/>
        <source>Scanner</source>
        <translation>スキャナー</translation>
    </message>
    <message>
        <location filename="../dialog_settings.cpp" line="25"/>
        <source>Help</source>
        <translation>ヘルプ</translation>
    </message>
    <message>
        <location filename="../dialog_settings.cpp" line="41"/>
        <source>System Default</source>
        <translation>システムのデフォルト</translation>
    </message>
    <message>
        <location filename="../dialog_settings.cpp" line="57"/>
        <source>Style</source>
        <translation>スタイル</translation>
    </message>
    <message>
        <location filename="../dialog_settings.cpp" line="57"/>
        <source>Desktop environment</source>
        <translation>デスクトップ環境</translation>
    </message>
    <message>
        <location filename="../dialog_settings.cpp" line="96"/>
        <source>%1 - %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialog_settings.cpp" line="152"/>
        <source>Application restart</source>
        <translation>アプリケーションの再起動</translation>
    </message>
    <message>
        <location filename="../dialog_settings.cpp" line="153"/>
        <source>Do you want to restart the application?
This setting will be reflected after restart.</source>
        <translation>アプリケーションを再起動しますか？
この設定は再起動後に反映されます。</translation>
    </message>
    <message>
        <location filename="../dialog_settings.cpp" line="169"/>
        <source>Are you sure you want to reset settings?
Restart the application.</source>
        <translation>設定をリセットしてもよろしいですか？
アプリケーションを再起動します。</translation>
    </message>
</context>
<context>
    <name>EditIP</name>
    <message>
        <location filename="../util/edit_ip.cpp" line="15"/>
        <source>Scan</source>
        <translation>スキャン</translation>
    </message>
    <message>
        <location filename="../util/edit_ip.cpp" line="21"/>
        <source>Auto detect</source>
        <translation>自動検出</translation>
    </message>
    <message>
        <location filename="../util/edit_ip.cpp" line="50"/>
        <location filename="../util/edit_ip.cpp" line="62"/>
        <location filename="../util/edit_ip.cpp" line="71"/>
        <source>IP range</source>
        <translation>IP範囲</translation>
    </message>
    <message>
        <location filename="../util/edit_ip.cpp" line="50"/>
        <source>No IP address specified</source>
        <translation>IPアドレスが指定されていません</translation>
    </message>
    <message>
        <location filename="../util/edit_ip.cpp" line="62"/>
        <source>IP address range is invalid</source>
        <translation>IPアドレス範囲が無効です</translation>
    </message>
    <message>
        <location filename="../util/edit_ip.cpp" line="71"/>
        <source>The end address should be larger than the start address</source>
        <translation>終了アドレスは開始アドレスよりも大きくする必要があります</translation>
    </message>
    <message>
        <location filename="../util/edit_ip.cpp" line="77"/>
        <source>000.000.000.000-000;_</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>EditMac</name>
    <message>
        <location filename="../util/edit_mac.cpp" line="9"/>
        <source>Auto detect</source>
        <translation>自動検出</translation>
    </message>
</context>
<context>
    <name>EditPath</name>
    <message>
        <location filename="../util/edit_path.cpp" line="9"/>
        <source>Open folder</source>
        <translation>フォルダを開く</translation>
    </message>
</context>
<context>
    <name>EditPathFileSelect</name>
    <message>
        <location filename="../util/edit_path_file_select.cpp" line="7"/>
        <source>Select file</source>
        <translation>ファイルを選択</translation>
    </message>
</context>
<context>
    <name>EditPathSelect</name>
    <message>
        <location filename="../util/edit_path_select.cpp" line="10"/>
        <location filename="../util/edit_path_select.cpp" line="20"/>
        <source>Select folder</source>
        <translation>フォルダーを選択</translation>
    </message>
</context>
<context>
    <name>FilterHeader</name>
    <message>
        <location filename="../util/filter_header.cpp" line="12"/>
        <source>Show or hide columns</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../util/filter_header.cpp" line="152"/>
        <source>Filter</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../util/filter_header.cpp" line="153"/>
        <source>Wildcards *, ? oder [] erlaubt. Zum Freistellen \ vor die Wildcard setzen</source>
        <translation>ワイルドカード *, ? もしくは [] を許可. ワイルドカードを無効にするには、ワイルドカードの前に \\ をつける</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="14"/>
        <source>QWakeOnLan</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="87"/>
        <source>Status</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="96"/>
        <source>toolBar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="125"/>
        <source>Add</source>
        <translation>追加</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="128"/>
        <source>Add device</source>
        <translation>デバイスの追加</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="131"/>
        <source>A</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="140"/>
        <source>Edit</source>
        <translation>編集</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="143"/>
        <source>Edit device</source>
        <translation>デバイスの編集</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="146"/>
        <source>E</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="155"/>
        <source>Delete</source>
        <translation>削除</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="158"/>
        <source>Delete device</source>
        <translation>デバイスの削除</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="161"/>
        <source>D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="176"/>
        <source>O</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="188"/>
        <source>Automatically adjust the column width</source>
        <translation>自動的に列幅を調整します</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="191"/>
        <source>W</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="254"/>
        <source>Open the RDP link</source>
        <translation>RDPリンクを開く</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="266"/>
        <location filename="../mainwindow.cpp" line="88"/>
        <location filename="../mainwindow.cpp" line="292"/>
        <location filename="../mainwindow.cpp" line="298"/>
        <source>Suspend</source>
        <translation>サスペンド</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="269"/>
        <source>S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="203"/>
        <source>Filter reset</source>
        <translation>フィルタリセット</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="170"/>
        <location filename="../mainwindow.ui" line="173"/>
        <source>Trun on</source>
        <translation>起動</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="185"/>
        <source>Auto size</source>
        <translation>自動サイズ</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="206"/>
        <source>Reset the filter</source>
        <translation>フィルターをリセットします</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="209"/>
        <source>R</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="221"/>
        <source>Settings</source>
        <translation>設定</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="224"/>
        <source>Show settings</source>
        <translation>設定を表示</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="227"/>
        <source>Alt+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="239"/>
        <source>Scan</source>
        <translation>スキャン</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="242"/>
        <source>Network scan</source>
        <translation>ネットワーク・スキャン</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="257"/>
        <source>L</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="47"/>
        <source>DB connection</source>
        <translation>DB接続</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="64"/>
        <source>Quit(&amp;Q)</source>
        <translation>終了(&amp;Q)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="87"/>
        <source>ID</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="87"/>
        <source>Device name</source>
        <translation>デバイス名</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="87"/>
        <source>Host</source>
        <translation>ホスト</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="269"/>
        <source>waiting for start-up</source>
        <translation>起動待ち</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="270"/>
        <source>waiting for start-up. Timeout in progress.
%1
%2</source>
        <translation>起動待ち… 進行中のタイムアウト
%1
%2</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="292"/>
        <source>This device is offline</source>
        <translation>このデバイスはオフラインです</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="298"/>
        <source>Suspend setting is disabled</source>
        <translation>サスペンド設定が無効になっています</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="405"/>
        <source>DB error</source>
        <translation>DBエラー</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="431"/>
        <source>MAC vendor list download</source>
        <translation>MACベンダーリスト ダウンロード</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="435"/>
        <source>Completed oui.txt download.</source>
        <translation>oui.txtのダウンロードが完了しました。</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="440"/>
        <location filename="../mainwindow.cpp" line="484"/>
        <source>Standard error: </source>
        <translation>標準出力エラー: </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="447"/>
        <source>Start oui.txt download...</source>
        <translation>oui.txtのダウンロードを開始します...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="454"/>
        <source>Version check</source>
        <translation>バージョンチェック</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="470"/>
        <source>Found a newer version.
</source>
        <translation>新しいバージョンが見つかりました。
</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="471"/>
        <source>Current version: %1</source>
        <translation>現在のバージョン: %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="472"/>
        <source>Latest version: %1
</source>
        <translation>最新バージョン: %1
</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="473"/>
        <source>Check the release page?</source>
        <translation>リリースページを確認しますか？</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="88"/>
        <location filename="../mainwindow.cpp" line="254"/>
        <source>MAC address</source>
        <translation>MACアドレス</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="251"/>
        <location filename="../mainwindow.cpp" line="88"/>
        <source>RDP link</source>
        <translation>RDPリンク</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="254"/>
        <source>Please set the MAC address</source>
        <translation>MACアドレスを設定してください</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="399"/>
        <source>DB connection error</source>
        <translation>DB接続エラー</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="400"/>
        <source>Syntax error</source>
        <translation>構文エラー</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="401"/>
        <source>Transaction error</source>
        <translation>トランザクションエラー</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="402"/>
        <source>Unknown error</source>
        <translation>未知のエラー</translation>
    </message>
</context>
<context>
    <name>RdpItem</name>
    <message>
        <location filename="../util/rdpItem.cpp" line="44"/>
        <source>Open the link</source>
        <translation>リンクを開く</translation>
    </message>
    <message>
        <location filename="../util/rdpItem.cpp" line="44"/>
        <source>File not found</source>
        <translation>ファイルが見つかりませんでした</translation>
    </message>
    <message>
        <location filename="../util/rdpItem.cpp" line="60"/>
        <source>all(*.*)</source>
        <translatorcomment>すべて</translatorcomment>
        <translation>すべて(*.*)</translation>
    </message>
    <message>
        <location filename="../util/rdpItem.cpp" line="62"/>
        <source>Open file</source>
        <translation>ファイルを開く</translation>
    </message>
    <message>
        <location filename="../util/rdpItem.cpp" line="83"/>
        <source>Import setting value</source>
        <translation>設定読み込み</translation>
    </message>
    <message>
        <location filename="../util/rdpItem.cpp" line="84"/>
        <source>Do you want to import the settings from the RDP file?</source>
        <translation>RDPファイルから設定を読込みますか？</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../util/settings.h" line="49"/>
        <location filename="../util/settings.h" line="52"/>
        <location filename="../util/settings.h" line="56"/>
        <source>%1/%2</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SqlModel</name>
    <message>
        <location filename="../util/sqlmodel.cpp" line="22"/>
        <location filename="../util/sqlmodel.cpp" line="45"/>
        <location filename="../util/sqlmodel.cpp" line="91"/>
        <location filename="../util/sqlmodel.cpp" line="215"/>
        <source>SELECT %1 FROM %2 </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../util/sqlmodel.cpp" line="25"/>
        <source>%1 GROUP BY %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../util/sqlmodel.cpp" line="30"/>
        <source>%1 LIMIT %2 OFFSET 0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../util/sqlmodel.cpp" line="35"/>
        <source>%1 LIMIT %2 OFFSET %3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../util/sqlmodel.cpp" line="59"/>
        <source>DELETE FROM %1 WHERE %2 in (%3);</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../util/sqlmodel.cpp" line="66"/>
        <source>DELETE FROM %1 WHERE %2;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../util/sqlmodel.cpp" line="94"/>
        <source>%1 GROUP BY %2 ORDER BY %2 %3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../util/sqlmodel.cpp" line="107"/>
        <source>SELECT count(*) AS records FROM %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../util/sqlmodel.cpp" line="162"/>
        <location filename="../util/sqlmodel.cpp" line="180"/>
        <source>%1=%2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../util/sqlmodel.cpp" line="163"/>
        <source>UPDATE %1 SET %2 WHERE %3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../util/sqlmodel.cpp" line="173"/>
        <source>INSERT INTO %1 (%2) VALUES(%3)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../util/sqlmodel.cpp" line="192"/>
        <source>SELECT currval(&apos;%1_%2_seq&apos;)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../util/sqlmodel.cpp" line="194"/>
        <source>SELECT * FROM &apos;%1&apos; WHERE &apos;%2&apos;=last_insert_rowid();</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../util/sqlmodel.cpp" line="203"/>
        <source>SELECT * FROM %1 WHERE %2=%3;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../util/sqlmodel.cpp" line="255"/>
        <source>TRUNCATE TABLE %1 %2;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../util/sqlmodel.cpp" line="257"/>
        <source>DELETE FROM %1;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../util/sqlmodel.cpp" line="258"/>
        <source>DELETE FROM sqlite_sequence WHERE name=&apos;%1&apos;;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../util/sqlmodel.cpp" line="259"/>
        <source>vacuum;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../util/sqlmodel.cpp" line="267"/>
        <source>select * from %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../util/sqlmodel.cpp" line="274"/>
        <source>INSERT INTO %1 (%2) VALUES(%3);</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../util/sqlmodel.h" line="134"/>
        <location filename="../util/sqlmodel.h" line="143"/>
        <location filename="../util/sqlmodel.h" line="146"/>
        <location filename="../util/sqlmodel.h" line="149"/>
        <source>&apos;%1&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../util/sqlmodel.h" line="160"/>
        <source>`%1`</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Suspend</name>
    <message>
        <location filename="../util/suspend.cpp" line="32"/>
        <source>Invalid command line argument</source>
        <translation>コマンドライン引数が無効です</translation>
    </message>
    <message>
        <location filename="../util/suspend.cpp" line="33"/>
        <source>Conflicting arguments given</source>
        <translation>指定された引数が競合しています</translation>
    </message>
    <message>
        <location filename="../util/suspend.cpp" line="34"/>
        <source>General runtime error</source>
        <translation>一般的なランタイムエラー</translation>
    </message>
    <message>
        <location filename="../util/suspend.cpp" line="35"/>
        <source>Unrecognized response from ssh (parse error)</source>
        <translation>sshからの認識できないレスポンス（解析エラー）</translation>
    </message>
    <message>
        <location filename="../util/suspend.cpp" line="36"/>
        <source>Invalid/incorrect password</source>
        <translation>パスワードが無効・不正</translation>
    </message>
    <message>
        <location filename="../util/suspend.cpp" line="37"/>
        <source>Host public key is unknown. sshpass exits without confirming the new key.</source>
        <translation>ホストの公開鍵は不明です。sshpassは、新しいキーを確認せずに終了します。</translation>
    </message>
    <message>
        <location filename="../util/suspend.cpp" line="38"/>
        <source>IP public key changed. sshpass exits without confirming the new key.</source>
        <translation>IP公開鍵が変更されました。 sshpassは、新しいキーを確認せずに終了します。</translation>
    </message>
    <message>
        <location filename="../util/suspend.cpp" line="103"/>
        <source>Successful suspend command</source>
        <translation>サスペンドコマンド成功</translation>
    </message>
    <message>
        <location filename="../util/suspend.cpp" line="108"/>
        <source>Exit code: %1</source>
        <translation>終了コード: %1</translation>
    </message>
    <message>
        <location filename="../util/suspend.cpp" line="112"/>
        <source>Standard error: </source>
        <translation>標準出力エラー: </translation>
    </message>
    <message>
        <location filename="../util/suspend.cpp" line="100"/>
        <source>Suspend %1</source>
        <translation>サスペンド %1</translation>
    </message>
    <message>
        <location filename="../util/suspend.cpp" line="131"/>
        <source>all(*.*)</source>
        <translation>すべて(*.*)</translation>
    </message>
    <message>
        <location filename="../util/suspend.cpp" line="133"/>
        <source>Open file</source>
        <translation>ファイルを開く</translation>
    </message>
</context>
</TS>
