#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "dialog_edit.h"
#include "dialog_scanner.h"
#include "dialog_settings.h"
#include "dialog_login.h"
#include "util/menu.h"
#include "util/rdpItem.h"
#include "util/tableview_delegate.h"
#include "util/suspend.h"

#include <QApplication>
#include <QDesktopWidget>
#include <QMessageBox>
#include <QTimer>
#include <QStyleFactory>
#include <QProgressDialog>
#include <QDomDocument>

MainWindow::MainWindow(QWidget *parent)
    : BaseMainWindow(parent)
    , settings(new Settings())
    , sqlModel(new SqlModel("device", "id"))
    , sqlModelView(new SqlModel("device", "id", "v_device"))
    , trayIcon(new QSystemTrayIcon(this))
    , trayIconMenu(new QMenu(this))
    , timerInterval(new QTimer)
    , timerWakeTimeout(new QTimer)
    , suspend (new Suspend(this))
    , delegate(new TableViewDelegate(this))
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // db error
    connect(sqlModel, &SqlModel::sqlError, this, &MainWindow::onSqlError);

    // local appDatePath
    QDir dir(appDataPath);
    if (!dir.exists())
        dir.mkpath(dir.absolutePath());

    // open database
    QSqlDatabase db(QSqlDatabase(QSqlDatabase::addDatabase("QSQLITE")));
    db.setDatabaseName(appDataPath + QDir::separator() + "wol.db");
    if (!db.open())
        showMessage(tr("DB connection"), { db.lastError().text() });

    // tray icon
    trayIcon->setIcon(QIcon(":/images/QWakeOnLan.png"));
    trayIcon->setToolTip("QWakeOnLan");
    connect(trayIcon, &QSystemTrayIcon::activated, [=] (QSystemTrayIcon::ActivationReason reason) {
        switch (reason) {
        case QSystemTrayIcon::Trigger:
        case QSystemTrayIcon::DoubleClick:
        case QSystemTrayIcon::MiddleClick:
            if (this->isVisible()) { if (watingId == -1) hide(); } else showNormal();
            break;
        default:;
        }
    });

    // tray context menu
    actionQuit = new QAction(QIcon::fromTheme("window-close"), tr("Quit(&Q)"), this);
    connect(actionQuit, &QAction::triggered, qApp, &QCoreApplication::quit);
    trayIconMenu->addAction(actionQuit);
    trayIcon->setContextMenu(trayIconMenu);
    trayIcon->show();

    // create table
    auto tabels = db.tables();
    if (!tabels.contains("device")) {
        sqlModel->execSqlFile(":/sql/device.sql");
        sqlModel->execSqlFile(":/sql/v_device.sql");
    }
    if (!tabels.contains("scanner"))
        sqlModel->execSqlFile(":/sql/scanner.sql");

    // toolBar
    setupToolBar(ui->toolBar);

    // tableView
    ui->tableView->setSqlModel(sqlModelView);
    ui->tableView->setItemDelegate(delegate);
    ui->tableView->initSql();
    static const QStringList headers = {
        tr("ID"), tr("Status"), tr("Device name"), tr("Host"),
        tr("MAC address"), tr("RDP link"), tr("Suspend")
    };
    ui->tableView->setHeaderLabels(headers);

    connect(ui->tableView, &TableViewSqlFilter::doubleClicked, [=] (const QModelIndex &/*index*/) {
        ui->actionWakeUp->trigger();
    });
    connect(ui->tableView, &TableViewSqlFilter::selectionChangedId, [=] (int /*id*/) {
        updateNeedActions();
    });
    connect(ui->tableView, &TableViewSqlFilter::mouseMove, [=] () {
        updateNeedActions();
    });

    // actions
    needSelectActions << ui->actionWakeUp << ui->actionEdit
                      << ui->actionDelete << ui->actionOpenLink << ui->actionSuspend;
    updateNeedActions();

    connect(ui->actionWakeUp, &QAction::triggered, [=] {
        auto ids = ui->tableView->getSelectedIds();
        foreach (auto id, ids) wolCommand(id);
    });
    connect(ui->actionOpenLink, &QAction::triggered, [=] {
        auto ids = ui->tableView->getSelectedIds();
        foreach (auto id, ids) {
            RdpItem rdp(sqlModel->getRecord(id), this);
            rdp.opneRdpFile();
        }
    });
    connect(ui->actionSuspend, &QAction::triggered, [=] {
        auto ids = ui->tableView->getSelectedIds();
        foreach (auto id, ids) suspendCommand(id);
    });
    connect(ui->actionAdd, &QAction::triggered, [=] {
        openEdit(-1);
    });
    connect(ui->actionEdit, &QAction::triggered, [=] {
        auto id = ui->tableView->getSelectedId();
        if (id) openEdit(id);
    });
    connect(ui->actionDelete, &QAction::triggered, [=] {
        auto ids = ui->tableView->getSelectedIds();
        if (!ids.count()) return;
        if (!deleteConfirm(this)) return;
        foreach (auto id, ids)
            sqlModel->deleteRecord(id);

        sqlModel->execSql("vacuum;");
        ui->tableView->reQuery();
    });
    connect(ui->actionScanner, &QAction::triggered, [=] {
        DialogScanner dlg(this);
        dlg.exec();
    });
    connect(ui->actionSetting, &QAction::triggered, [=] {
        DialogSettings dlg(this);
        if (dlg.exec() == QDialog::Accepted)
            readSetting();
    });
    connect(ui->actionRestFilter, &QAction::triggered, ui->tableView, &TableViewSqlFilter::resetFiler);
    connect(ui->actionAutoSize, &QAction::triggered, this, [=] () {
        ui->tableView->autoSizeColRow();

        auto szTable = ui->tableView->getContentsSize();
        QSize szInner(szTable.width(), szTable.height() + ui->toolBar->height() + ui->frameFooter->height());
        QRect rcScreen = QApplication::desktop()->screenGeometry(this);
        if ((szInner.height() + pos().y()) > rcScreen.height())
            szInner.setHeight(rcScreen.height() - pos().y());
        resize(szInner);
    });

    QWidget *spacer = new QWidget();
    spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    ui->toolBar->addWidget(spacer);
    ui->toolBar->addSeparator();
    ui->toolBar->addAction(ui->actionScanner);
    ui->toolBar->addSeparator();
    ui->toolBar->addAction(ui->actionSetting);

    // suspend message
    connect(suspend, &Suspend::openFileRespons, this, &MainWindow::showMessage);

    // interval timer
    timerInterval->setSingleShot(true);
    timerInterval->setTimerType(Qt::PreciseTimer);
    timerInterval->start();
    connect(timerInterval, &QTimer::timeout, this, &MainWindow::onIntervalTimer);

    // wakeTimeout
    timerWakeTimeout->setSingleShot(true);
    timerWakeTimeout->setTimerType(Qt::PreciseTimer);
    connect(timerWakeTimeout, &QTimer::timeout, [=] {
        watingId = -1;
    });

    // load setting
    readSetting();
    restoreGeometry(settings->valueObj(this, "geometry").toByteArray());
    restoreState(settings->valueObj(this, "windowState").toByteArray());
    auto state = settings->valueObj(this, "columnsSave", "").toByteArray();
    if (!state.isEmpty())
        ui->tableView->restoreState(state);
}

MainWindow::~MainWindow()
{
    settings->setValueObj(this, "columnsSave", ui->tableView->saveState());
    settings->setValueObj(this, "geometry", saveGeometry());
    settings->setValueObj(this, "windowState", saveState());

    delete settings;
    delete sqlModel;
    delete sqlModelView;
    delete delegate;
    delete trayIcon;
    delete trayIconMenu;
    delete timerInterval;
    delete timerWakeTimeout;
    delete suspend;
    delete ui;

    QSqlDatabase::database().close();
}

void MainWindow::onIntervalTimer()
{
    bool isUpdate = false;
    QSqlQuery query(sqlModel->getSqlSelect());

    while (query.next()) {
        auto rs = query.record();
        int id = rs.value("id").toInt();
        Host host(rs, pingWait); // with ping

        // update status
        if (host.isActive() != rs.value("active").toBool()) {
            sqlModel->setValueClear();
            sqlModel->setValue("active", host.isActive());
            sqlModel->saveValues(id);
            isUpdate = true;
        }

        // open link (remote desktop)
        if (id == watingId && host.isActive()) {
            timerWakeTimeout->stop();
            ui->actionOpenLink->trigger();
            watingId = -1;
        }
    }

    // requery
    if (isUpdate)
        ui->tableView->reQuery();

    timerInterval->start();
}

void MainWindow::wolCommand(int id)
{
    auto rs = sqlModel->getRecord(id);
    auto name = rs.value("name").toString();

    Host host(rs, pingWait);
    if (!host.isActive()) {
        if (!host.wakeOnLan()) {
            showMessage(tr("MAC address"), { name, tr("Please set the MAC address") });
            return;
        }
    }

    RdpItem rdp(rs);
    if (opneLink && watingId == -1 && rdp.isFileExsit()) {
        watingId = id;
        timerWakeTimeout->start();

        QProgressDialog progDlg(this);
        connect(&progDlg, &QProgressDialog::canceled, this, [=] () {
            timerWakeTimeout->stop();
            watingId = -1;
        });
        progDlg.setWindowTitle(tr("waiting for start-up"));
        progDlg.setLabelText(tr("waiting for start-up. Timeout in progress.\n%1\n%2")
                                 .arg(name, rs.value("host").toString()));
        progDlg.setRange(0, timerWakeTimeout->interval());
        progDlg.setWindowModality(Qt::WindowModal);
        progDlg.show();
        while (timerWakeTimeout->isActive()) {
            progDlg.setValue(timerWakeTimeout->interval() - timerWakeTimeout->remainingTime());
            QApplication::processEvents();
        }
        progDlg.hide();
        timerWakeTimeout->stop();
        watingId = -1;
    }
}

void MainWindow::suspendCommand(int id)
{
    auto rs = sqlModel->getRecord(id);
    auto name = rs.value("name").toString();

    Host host(rs, pingWait);
    if (!host.isActive()) {
        showMessage(tr("Suspend"), { name, tr("This device is offline") });
        return;
    }

    suspend->setValue(rs);
    if (suspend->getType() == SuspendType::none) {
        showMessage(tr("Suspend"), { name, tr("Suspend setting is disabled") });
        return;
    };

    if (!suspend->isCheckLoginInfo()) {
        DialogLogin dlg(name, rs.value("user").toString(), rs.value("pass").toString(), this);

        if (dlg.exec() != QDialog::Accepted) return;

        if (dlg.isSave) {
            sqlModel->setValueClear();
            sqlModel->setValue("user", dlg.user);
            sqlModel->setValue("pass", suspend->getDecryptedPass(dlg.pass));
            sqlModel->saveValues(id);
        }
        suspend->opneFile(dlg.user, dlg.pass);
        return;
    }

    suspend->opneFile();
}

void MainWindow::openEdit(QWidget *parent /*= nullptr*/, int id /*= -1*/, Host *data /*= nullptr*/)
{
    DialogEdit dlg(parent, id, data);
    if (dlg.exec() == QDialog::Accepted)
        ui->tableView->reQuery();
}

void MainWindow::updateNeedActions()
{
    int id = ui->tableView->getSelectedId();
    foreach (auto action, needSelectActions)
        action->setEnabled((id != -1));
    if (id == -1) return;

    auto rs = sqlModel->getRecord(id);
    auto isActive = rs.value("active").toBool();
    auto suspendType = rs.value("suspendType").toInt();
    auto linkFile = rs.value("linkFile").toString();
    auto mac = rs.value("mac").toString();
    ui->actionWakeUp->setEnabled(!isActive && !mac.isEmpty());
    ui->actionSuspend->setEnabled(isActive && suspendType != 0);
    ui->actionOpenLink->setEnabled(!linkFile.isEmpty() && isActive);
}

void MainWindow::showMessage(const QString &title, const QStringList &messages,
    QSystemTrayIcon::MessageIcon icon /*=QSystemTrayIcon::Critical*/)
{
    if (trayIcon->supportsMessages()) {
        trayIcon->showMessage(title, messages.join("\n"), icon);
        return;
    }
    if (QSystemTrayIcon::Critical == icon)
        QMessageBox::critical(this, title, messages.join("\n"));
}

void MainWindow::on_tableView_customContextMenuRequested(const QPoint &pos)
{
    Menu menu(this);

    updateNeedActions();

    menu.addAction(ui->actionWakeUp);
    menu.addAction(ui->actionOpenLink);
    menu.addAction(ui->actionSuspend);
    menu.addSeparator();
    menu.addAction(ui->actionAdd);
    menu.addAction(ui->actionEdit);
    menu.addAction(ui->actionDelete);
    menu.addSeparator();
    menu.addAction(ui->actionRestFilter);
    menu.addAction(ui->actionAutoSize);
    menu.addSeparator();
    menu.addAction(ui->actionScanner);
    menu.addSeparator();
    menu.addAction(ui->actionSetting);
    menu.exec(ui->tableView->viewport()->mapToGlobal(pos));
}

void MainWindow::readSetting()
{
    auto style = settings->value("style").toString();
    qApp->setStyle(QStyleFactory::create(style));

    timerInterval->setInterval(settings->value("pingInterval").toInt());
    timerWakeTimeout->setInterval(settings->value("wakeTimeout").toInt() * 6000);
    pingWait = settings->value("pingWaitTime").toInt();
    opneLink = settings->value("opneLink").toBool();

    settings->makePaths();
    suspend->setupScriptFiles();
    downloadOuiFile();
    checkVersion();
}

void MainWindow::onSqlError(const QString &sql, const QSqlError &lastError)
{
    QStringList messages;
    switch (lastError.type()) {
        case QSqlError::NoError: return;
        case QSqlError::ConnectionError : messages << tr("DB connection error"); break;
        case QSqlError::StatementError  : messages << tr("Syntax error"); break;
        case QSqlError::TransactionError: messages << tr("Transaction error"); break;
        default: messages << tr("Unknown error");
    }
    messages << "" << "Query:" << sql << " Message：" << lastError.text();
    showMessage(tr("DB error"), messages);
    qDebug() << messages.join("\n");
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if (trayIcon->isVisible()) {
        if (watingId == -1) hide();
        event->ignore();
    }
}

void MainWindow::showEvent(QShowEvent */*event*/)
{
    timerInterval->start();
}

void MainWindow::hideEvent(QHideEvent */*event*/)
{
    timerInterval->stop();
}

void MainWindow::downloadOuiFile()
{
    if (settings->value("chkOuiCompleted").toBool()) return;

    auto title = tr("MAC vendor list download");
    auto *ps = new QProcess;
    connect(ps, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), [=] (int exitCode, QProcess::ExitStatus exitStatus) {
        if (exitStatus == QProcess::NormalExit && exitCode == 0) {
            showMessage(title, { tr("Completed oui.txt download.") }, QSystemTrayIcon::Information);
            settings->setValue("chkOuiCompleted", true);
        } else {
            auto result = QString::fromUtf8(ps->readAllStandardError());
            if (!result.isEmpty())
                showMessage(title, { tr("Standard error: "), result });
        }
        ps->deleteLater();
    });

    auto url = settings->value("cmbOuiUrl").toString();
    ps->start("wget", { url, "-O", settings->value("editOuiFile").toString() });
    showMessage(title, { tr("Start oui.txt download...") }, QSystemTrayIcon::Information);
}

void MainWindow::checkVersion()
{
    if (!settings->value("chkVersionCheck").toBool()) return;

    auto title = tr("Version check");
    auto filePath = appDataPath + QDir::separator() + "rssTags.xml";
    QString projectUrl = "https://gitlab.com/oioi555/QWakeOnLan/-/";

    auto *ps = new QProcess;
    connect(ps, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), [=] (int exitCode, QProcess::ExitStatus exitStatus) {
        if (exitStatus == QProcess::NormalExit && exitCode == 0) {
            QFile file(filePath);
            if (file.open(QFile::ReadOnly)) {
                auto current = versionCast(qApp->applicationVersion());
                auto versions = getRssVersions(QString::fromUtf8(file.readAll()));
                if (!versions.count()) return;

                auto latest = versionCast(versions[0]);
                if (latest > current) {
                    QStringList messages;
                    messages << tr("Found a newer version.\n");
                    messages << tr("Current version: %1").arg(qApp->applicationVersion());
                    messages << tr("Latest version: %1\n").arg(versions[0]);
                    messages << tr("Check the release page?");

                    if (QMessageBox::Ok == QMessageBox::information(this, title, messages.join("\n"), QMessageBox::Ok|QMessageBox::Cancel)) {
                        QProcess::startDetached("xdg-open", { projectUrl + "releases" });
                    }
                }
                file.close();
            }
        } else {
            auto result = QString::fromUtf8(ps->readAllStandardError());
            if (!result.isEmpty())
                showMessage(title, { tr("Standard error: "), result });
        }
        ps->deleteLater();
    });

    ps->start("wget", { projectUrl + "tags?format=atom", "-O", filePath });
}

QStringList MainWindow::getRssVersions(const QString xmlStr)
{
    QStringList result;
    QDomDocument xml;
    xml.setContent(xmlStr);
    QDomElement entry = xml.documentElement().firstChild().toElement();
    while(!entry.isNull()) {
        if (entry.tagName() == "entry") {
            QDomElement child = entry.firstChild().toElement();
            while (!child.isNull()) {
                if (child.tagName() == "title")
                    result << child.text();
                child = child.nextSibling().toElement();
            }
        }
        entry = entry.nextSibling().toElement();
    }
    return result;
}

double MainWindow::versionCast(const QString &versionStr)
{
    double result = 0;
    QString version = versionStr;
    QRegExp reg;
    reg.setPattern("^v|-.*");
    version.replace(reg, "");

    auto digit = version.split(".");
    result = digit[0].toInt() + (digit[1].toDouble() * 0.1) + (digit[2].toDouble() * 0.01);

    return result;
}
