#include "dialog_edit.h"
#include "ui_dialog_edit.h"
#include "util/rdpItem.h"
#include "util/suspend.h"

#include <QFileDialog>
#include <QMessageBox>
#include <QPixmap>
#include <QSqlRelationalDelegate>
#include <QLineEdit>

DialogEdit::DialogEdit(QWidget *parent, int id /*= -1*/, Host *host /*= nullptr*/) : QDialog(parent)
  , id((id == 0)? -1 : id)
  , tableName("device")
  , idName("id")
  , addMode((id == -1))
  , model(new QSqlRelationalTableModel(this))
  , mapper(new QDataWidgetMapper(this))
  , rdp(new RdpItem(this))
  , suspend(new Suspend(this))
  , main(qobject_cast<MainWindow*>(parent))
  , ui(new Ui::DialogEdit)
{
    ui->setupUi(this);

    setWindowTitle(tr("Device：%1").arg(addMode? tr("Add") : tr("Edit")));
    ui->laIcon->setPixmap(QIcon::fromTheme("computer").pixmap(64, 64));

    // combobox
    rdp->setupRdpComboItems(ui->cmbRdpLinkType);
    suspend->setupComboItems(ui->cmbSuspendType);

    // model
    model->setTable(tableName);
    model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    model->setFilter(QString("%1 = %2").arg(idName).arg(id));
    model->select();

    // mapping
    mapper->setModel(model);
    mapper->setItemDelegate(new QSqlRelationalDelegate(this));

    if(!addMode) mapper->addMapping(ui->editId, model->fieldIndex(idName));
    mapper->addMapping(ui->editName, model->fieldIndex("name"));
    mapper->addMapping(ui->editIP, model->fieldIndex("host"));
    mapper->addMapping(ui->editMac, model->fieldIndex("mac"));
    mapper->addMapping(ui->editLink, model->fieldIndex("linkFile"));
    mapper->addMapping(ui->cmbRdpLinkType, model->fieldIndex("rdpType"), "currentIndex");
    mapper->addMapping(ui->cmbSuspendType, model->fieldIndex("suspendType"), "currentIndex");
    mapper->addMapping(ui->editSuspendFile, model->fieldIndex("suspendFile"));
    mapper->addMapping(ui->editUser, model->fieldIndex("user"));
    mapper->addMapping(ui->editPass, model->fieldIndex("pass"));
    mapper->addMapping(ui->editPort, model->fieldIndex("port"));

    if (addMode) {  // add
        model->insertRow(model->rowCount(QModelIndex()));
        ui->editId->setPlaceholderText(tr("add new"));
    }
    mapper->toLast();

    connect(ui->cmbSuspendType, &QComboBox::currentTextChanged, this, [=] () {
        int index = ui->cmbSuspendType->currentIndex();
        auto type = suspend->castType(index);
        if (type == SuspendType::Other)
            on_editSuspendFile_actionClicked();
        else
            ui->editSuspendFile->setText(suspend->getFileName(index));
        updateSuspendCtlsEnabled();
        main->getSettings()->setValue("suspendType", ui->cmbSuspendType->currentIndex());
    });

    // initial value
    if (addMode) {
        ui->cmbRdpLinkType->setCurrentIndex(main->getSettings()->value("rdpType").toInt());
        ui->cmbSuspendType->setCurrentIndex(main->getSettings()->value("suspendType").toInt());
        if (addMode && host != nullptr) {
            ui->editIP->setText(host->getIp());
            ui->editName->setText(host->getName());
            ui->editMac->setText(host->getMac());
        }
    }

    connect(ui->editMac, &EditMac::actionClicked, this, [=] () {
        Host host(50, ui->editIP->text());
        if (host.scanMac()) ui->editMac->setText(host.getMac());
    });
    connect(ui->cmbRdpLinkType, &QComboBox::currentTextChanged, this, [=] () {
        on_editLink_actionClicked();
        main->getSettings()->setValue("rdpType", ui->cmbRdpLinkType->currentIndex());
    });
    updateRdpFileEnabled();

    auto pass = ui->editPass->text();
    ui->editPass->setText(suspend->getClearPass(pass));

    suspendControls << ui->editSuspendFile << ui->editUser << ui->editPass << ui->editPort;
    updateSuspendCtlsEnabled();

    // setting
    restoreGeometry(main->getSettings()->valueObj(this, "geometry").toByteArray());
}

DialogEdit::~DialogEdit()
{
    main->getSettings()->setValueObj(this, "geometry", saveGeometry());

    delete rdp;
    delete suspend;
    delete model;
    delete mapper;
    delete ui;
}

void DialogEdit::on_buttonBox_accepted()
{
    auto name = ui->editName->text();
    if (name.isEmpty()) {
        QMessageBox::critical(this, tr("validation"), tr("Missning device name"));
        return;
    }
    auto ip = ui->editIP->text();
    if (ip.isEmpty()) {
        QMessageBox::critical(this, tr("validation"), tr("Missning Host"));
        return;
    }
    rdp->setValue(ui->cmbRdpLinkType->currentIndex(), ui->editLink->text());
    if (rdp->getRdpType() != RdpType::None && !rdp->isFileExsit()) {
        QMessageBox::critical(this, tr("validation"), tr("Missning RDP File"));
        return;
    }

    auto mac = ui->editMac->text();
    if (mac == ":::::") {
        ui->editMac->setInputMask("");
        ui->editMac->setText("");
    }

    // pass encrypt
    auto pass = ui->editPass->text();
    ui->editPass->setText(suspend->getDecryptedPass(pass));

    mapper->submit();

    model->database().transaction();
    if (model->submitAll()) {
        model->database().commit();
        accept(); return;
    }
    model->database().rollback();

    QString message = tr("DB Error: %1").arg(model->lastError().text());
    QMessageBox::warning(this, tr("DB Error"), message);
}

void DialogEdit::on_editLink_actionClicked()
{
    updateRdpFileEnabled();
    auto currentIndex = ui->cmbRdpLinkType->currentIndex();
    if (!rdp->selectRdpFile(currentIndex)) return;

    ui->editLink->setText(rdp->getFileName());
    auto newIndex = rdp->getRdpTypeIndex();
    if (newIndex != currentIndex) {
        ui->cmbRdpLinkType->blockSignals(true);
        ui->cmbRdpLinkType->setCurrentIndex(newIndex);
        ui->cmbRdpLinkType->blockSignals(false);
    }

    Host host;
    if (rdp->readRdpFile(host)) {
        ui->editName->setText(host.getName());
        ui->editIP->setText(host.getIp());
        if (host.scanMac()) ui->editMac->setText(host.getMac());
    }
}

void DialogEdit::on_editSuspendFile_actionClicked()
{
    auto currentIndex = ui->cmbSuspendType->currentIndex();
    if (!suspend->selectFile()) return;

    ui->editSuspendFile->setText(suspend->getFileName());
    auto newIndex = suspend->getTypeIndex();
    if (newIndex != currentIndex) {
        ui->cmbSuspendType->blockSignals(true);
        ui->cmbSuspendType->setCurrentIndex(newIndex);
        ui->cmbSuspendType->blockSignals(false);
    }
}

void DialogEdit::updateRdpFileEnabled()
{
    auto currentIndex = ui->cmbRdpLinkType->currentIndex();
    if (currentIndex == 0) {
        ui->editLink->setText("");
        ui->editLink->setEnabled(false);
        return;
    }
    ui->editLink->setEnabled(true);
}

void DialogEdit::updateSuspendCtlsEnabled()
{
    auto type = suspend->castType(ui->cmbSuspendType->currentIndex());
    foreach (auto ctl, suspendControls) {
        ctl->setEnabled((type != SuspendType::none));
        if (type == SuspendType::none)
            ctl->setText("");
    }
}

