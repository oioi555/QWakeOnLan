CREATE TABLE "scanner" (
    "id"	INTEGER,
    "host"	TEXT,
    "mac"	TEXT,
    "vendor"	TEXT,
    PRIMARY KEY("id" AUTOINCREMENT)
);
