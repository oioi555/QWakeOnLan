CREATE TABLE "device" (
    "id"	INTEGER,
    "active"	INTEGER,
    "name"	TEXT,
    "host"	TEXT,
    "mac"	BLOB,
    "rdpType"	INTEGER,
    "linkFile"	TEXT,
    "suspendType"	INTEGER,
    "suspendFile"	TEXT,
    "user"	TEXT,
    "pass"	TEXT,
    "port"	TEXT,
    PRIMARY KEY("id" AUTOINCREMENT)
);
