#ifndef EDIT_MAC_H
#define EDIT_MAC_H

#include <QLineEdit>

class EditMac : public QLineEdit
{
    Q_OBJECT

public:
    explicit EditMac(QWidget *parent = nullptr);

signals:
    void actionClicked();
};

#endif // EDIT_MAC_H
