#include "host.h"
#include "settings.h"

#include <QUdpSocket>
#include <QFileInfo>
#include <QHostInfo>
#include <QProcess>
#include <QStandardPaths>

bool Host::ping() {
    QProcess ps;
    ps.start("ping", QStringList() << "-c" << "1" << host);
    ps.waitForFinished(pingWait);
    auto result = QString::fromLocal8Bit(ps.readAllStandardOutput());
    active = !result.isEmpty() && (-1 != QString(result).indexOf("ttl", 0, Qt::CaseInsensitive));
    ps.close();
    return active;
}

void Host::scan() {
    if (!scanMac()) return;
    scanVender();
}

bool Host::scanMac()
{
    if (!ping()) return false;
    QProcess ps;
    ps.start("ip", { "n", "s", host });
    ps.waitForFinished();
    auto result = QString::fromLocal8Bit(ps.readAllStandardOutput()).split(" ");
    if (result.count() > 4)
        mac = result.at(4).toUpper();
    ps.close();
    return true;
}

bool Host::scanVender()
{
    auto venderId = getVendorId();
    if (venderId.isEmpty()) return false;

    QProcess ps;
    ps.start("grep", { venderId, QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/oui.txt" });
    ps.waitForFinished();

    auto venderNames = QString::fromUtf8(ps.readAllStandardOutput()).split("(hex)\t\t");
    if (venderNames.count() > 1)
        vendorName = venderNames.at(1).trimmed();
    ps.close();
    return true;
}

QString Host::getVendorId()
{
    QString result;
    auto venderId = mac.split(":");
    if (venderId.count() == 6) {
        for (int i = 0; i < 3; i++) venderId.removeLast();
        result = venderId.join("-");
    }
    return result;
}

void Host::loadOuiFilePath()
{
    Settings settings;
    ouiFilePath = settings.value("editOuiFile").toString();
}

bool Host::wakeOnLan()
{
    QByteArray macHexes;
    bool isOk = false;
    foreach (auto value, mac.split(":")) {
        macHexes.append(value.toInt(&isOk, 16));
        if (!isOk) return false;
    }
    if (macHexes.length() != 6) return false;

    QByteArray magicPacket;
    for (int i = 0; i < 6; i++) magicPacket.append(0xFF);
    for (int i = 0; i < 16; i++) foreach (auto hex, macHexes) magicPacket.append(hex);

    QUdpSocket udpSocket;
    udpSocket.writeDatagram(magicPacket, magicPacket.length() /*102*/, QHostAddress("255.255.255.255"), 9);
    return true;
}

