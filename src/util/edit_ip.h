#ifndef EDIT_IP_H
#define EDIT_IP_H

#include <QLineEdit>

class EditIP : public QLineEdit
{
    Q_OBJECT

public:
    explicit EditIP(QWidget *parent = nullptr);
    ~EditIP() override {}

    void setAutoRange();
    bool parseIP();
    int getIpMin() const {return ipMin;}
    int getIpMax() const {return ipMax;}
    QString getIp(int value) {
        QStringList result = ipAddress;
        result[3] = QString::number(value);
        return result.join(".");
    }
signals:
    void actionSearchClicked();

private:
    int ipMin;
    int ipMax;
    QStringList ipAddress;

    // QWidget interface
protected:
    virtual void showEvent(QShowEvent *event) override;
};

#endif // EDIT_IP_H
