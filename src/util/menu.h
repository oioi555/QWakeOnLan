#ifndef MENU_H
#define MENU_H

#include <QMenu>
#include <QMouseEvent>
#include <QToolTip>

class Menu : public QMenu
{
    Q_OBJECT

public:
    explicit Menu(QWidget *parent = nullptr) : QMenu(parent){}
    bool event (QEvent * e)
    {
        const QHelpEvent *helpEvent = static_cast <QHelpEvent *>(e);
         if (helpEvent->type() == QEvent::ToolTip && activeAction() != nullptr)
              QToolTip::showText(helpEvent->globalPos(), activeAction()->toolTip());
         else
              QToolTip::hideText();
         return QMenu::event(e);
    }
};
#endif // MENU_H
