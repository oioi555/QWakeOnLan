#include "tableview_sqlfilter.h"

#include <QApplication>
#include <QMouseEvent>
#include <QDrag>
#include <QMimeData>
#include <QScrollBar>

TableViewSqlFilter::TableViewSqlFilter(QWidget *parent): QTableView(parent)
    , proxyModel(new SortFilterProxyModel(this))
    , header(new FilterHeader())
    , idColumn(0) // id列の番号
{
    // 設定
    this->setSelectionMode(QAbstractItemView::ExtendedSelection);   // 複数行選択
    this->setSelectionBehavior(QAbstractItemView::SelectRows);      // 行選択
    this->setEditTriggers(QAbstractItemView::NoEditTriggers);       // 編集不可の設定
    this->setAlternatingRowColors(true);                            // 交互に行の色を変える
    this->setWordWrap(true);
    this->verticalHeader()->hide();

    // ヘッダー
    connect(header, &FilterHeader::stringFilterChanged, proxyModel, &SortFilterProxyModel::updateStringFilter);
    connect(header, &FilterHeader::stateFilterChanged, proxyModel, &SortFilterProxyModel::updateStateFilter);
    connect(header, &FilterHeader::stringFilterChanged,this, &TableViewSqlFilter::updateFilter);
    connect(header, &FilterHeader::headerResized, this, [=] (int logical, int oldSize, int newSize) {
        emit headerResized(logical, oldSize, newSize);
    });
    connect(header, &FilterHeader::columnsStateChanged, this, [=] {
        emit columnsStateChanged();
    });
    setMouseTracking(true);
}

TableViewSqlFilter::~TableViewSqlFilter() {
    delete proxyModel;
    delete header;
}

// 初期化
void TableViewSqlFilter::initSql()
{
    queryModel = sqlModel->getModel(SelectType::selectWhere);
    proxyModel->setSourceModel(queryModel);

    this->setHorizontalHeader(header);
    this->setSortingEnabled(true);                              // SqlでソートするとGUIのソートが無視される
    this->setModel(proxyModel);

    // id列を保存しておく
    QString idName = sqlModel->getIdName();
    for (int col = 0 ; col < queryModel->columnCount() ; col++) {
        if (queryModel->record().fieldName(col) == idName) {
            idColumn = col; break;
        }
    }
    updateFilter();
}

// 再クエリー
void TableViewSqlFilter::reQuery(bool useLastQuery /*= true*/)
{
    // 初期化されているか確認
    if (sqlModel->getSqlLast().isEmpty()) {
        initSql();
        return;
    }

    QModelIndexList selectedRows = this->selectionModel()->selectedRows();
    int selectRow = (selectedRows.count())? selectedRows[0].row() : -1;

    SelectType type = (useLastQuery)? SelectType::lastSQL : SelectType::selectWhere;
    queryModel = sqlModel->getModel(type);

    if (selectRow != -1) this->selectRow(selectRow);

    updateFilter();
}

void TableViewSqlFilter::setHeaderLabels(const QStringList &names)
{
    for (int i = 0 ; i < names.count() ; i++)
        model()->setHeaderData(i, Qt::Horizontal, names[i], Qt::DisplayRole);
}

auto TableViewSqlFilter::isSelected() -> bool
{
    return (getSelectedId() != -1);
}

void TableViewSqlFilter::selectionChanged(const QItemSelection &selected, const QItemSelection &deselected)
{
    if (selected.count() > 0) {
        int oldid = this->selectedId;
        int newid = getSelectedId();
        if ((newid != -1 || oldid != -1) && (oldid != newid))
            emit selectionChangedId(newid);
    }
    QTableView::selectionChanged(selected, deselected);
}

auto TableViewSqlFilter::getSelectedId() -> int
{
    int id = -1;
    auto selectedIds = getSelectedIds();
    if (selectedIds.count() > 0)
        id = selectedIds[0];

    return id;
}

bool TableViewSqlFilter::setSelectId(const int &selectID)
{
    if (selectID == -1) return false;
    for (int row = 0;  row < this->model()->rowCount(); ++row) {
        auto index = this->model()->index(row, idColumn);
        if (selectID == index.data().toInt()) {
            setCurrentIndex(index); return true;
        }
    }
    return false;
}

auto TableViewSqlFilter::getSelectedIds() -> QList<int>
{
    QList<int> result;
    QModelIndexList selectedRows = this->selectionModel()->selectedRows();
    int rowCount = selectedRows.count();
    if (rowCount == 0) return result;

    foreach (const QModelIndex &index ,selectedRows)
        result << this->model()->index(index.row(), idColumn).data().toInt();
    return result;
}

QSize TableViewSqlFilter::getContentsSize()
{
    auto szHeader = header->getHeaderSize();
    int width = szHeader.width() + this->verticalScrollBar()->width();

    int rowHeight = this->rowHeight(0) * sqlModel->getRecordCount();
    int height = szHeader.height() + rowHeight + this->horizontalScrollBar()->height();
    return QSize(width, height);
}

void TableViewSqlFilter::updateFilter()
{
    emit updateFilterEvent(getTitleFilters());
}

void TableViewSqlFilter::horizontalScrollbarValueChanged(int )
{   // 横スクロールするとフィルターボックスの位置がズレる
    header->fixWidgetPositions();
}
