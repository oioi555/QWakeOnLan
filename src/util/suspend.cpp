#include "suspend.h"
#include "settings.h"

#include <QComboBox>
#include <QFileDialog>
#include <QDebug>
#include <QApplication>
#include <QHash>

#include <qblowfish/qblowfish.h>
// Random key generated at http://www.random.org/bytes/
#define KEY_HEX "48500eda5ffc55a8cf4b10d9103496e3"

static const QString linuxSh = "linux-ssh.sh";
static const QString windowsSh = "win-ssh.sh";

Suspend::Suspend(QWidget *parent) : parent(parent)
{
    settings = new Settings;
    scriptPath = settings->value("editPathSuspend").toString();

    suspendTypeNames = {
        {"None", QIcon::fromTheme("dialog-cancel"), ""},
        {"Linux", QIcon::fromTheme("preferences-system-linux"), linuxSh},
        {"Windows", QIcon::fromTheme("im-msn"), windowsSh},
        {"Other", QIcon::fromTheme("akonadiconsole"), ""}
    };

    // https://sourceforge.net/projects/sshpass/
    // reference 'man sshpass'
    exitCodeMessages[0] = tr(""); // Windows: Exit code on success cannot be obtained
    exitCodeMessages[1] = tr("Invalid command line argument");
    exitCodeMessages[2] = tr("Conflicting arguments given");
    exitCodeMessages[3] = tr("General runtime error");
    exitCodeMessages[4] = tr("Unrecognized response from ssh (parse error)");
    exitCodeMessages[5] = tr("Invalid/incorrect password");
    exitCodeMessages[6] = tr("Host public key is unknown. sshpass exits without confirming the new key.");
    exitCodeMessages[7] = tr("IP public key changed. sshpass exits without confirming the new key.");
}

Suspend::~Suspend()
{
    delete settings;
}

void Suspend::setupScriptFiles()
{
    foreach (auto fileName, QStringList() << linuxSh << windowsSh) {
        this->suspendFile = fileName;
        if (isFileExsit()) continue;
        QFile::copy(QString(":/script/%1").arg(fileName), getFullPath());
        QProcess::startDetached("chmod", { "+x", getFullPath() });
    }
}

void Suspend::setupComboItems(QComboBox *cmb)
{
    cmb->clear();
    for (int i = 0; i < suspendTypeNames.count(); i++)
        cmb->insertItem(i, suspendTypeNames[i].icon, suspendTypeNames[i].name);
}

QIcon Suspend::getIcon(int type, const QString fileName)
{
    suspendType = castType(type);
    suspendFile = fileName;
    if (!isFileExsit()) return QIcon::fromTheme("error");
    return suspendTypeNames[(int)suspendType].icon;
}

bool Suspend::isCheckLoginInfo()
{
    return !(user.isEmpty() || pass.isEmpty());
}

QString Suspend::getClearPass(const QString &decryptedPass)
{
    QBlowfish bf(QByteArray::fromHex(KEY_HEX));
    bf.setPaddingEnabled(true);
    QByteArray decryptedBa = bf.decrypted(QByteArray::fromBase64(decryptedPass.toUtf8()));
    return QString::fromUtf8(decryptedBa.constData(), decryptedBa.size());
}

QString Suspend::getDecryptedPass(const QString &clearPass)
{
    QBlowfish bf(QByteArray::fromHex(KEY_HEX));
    bf.setPaddingEnabled(true);
    QByteArray cipherText = bf.encrypted(clearPass.toUtf8()).toBase64();
    return QString::fromUtf8(cipherText);
}

void Suspend::opneFile(const QString &user /*= ""*/, const QString &pass /*= ""*/)
{
    QString sendUser = user.isEmpty()? this->user : user;
    QString sendPass = pass.isEmpty()? getClearPass(this->pass) : pass;

    QProcess *ps = new QProcess;
    connect(ps, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), [=] (int exitCode, QProcess::ExitStatus exitStatus) {
        QStringList messages;
        QString title = tr("Suspend %1").arg(name);
        if (exitStatus == QProcess::NormalExit) {
            if (exitCode == 0) {
                emit openFileRespons(title, { tr("Successful suspend command") }, QSystemTrayIcon::Information);
                ps->deleteLater(); return;
            }
            auto exitCodeMessage = exitCodeMessages[exitCode];
            if (!exitCodeMessage.isEmpty())
                messages << tr("Exit code: %1").arg(exitCode) << exitCodeMessage;
        }
        auto result = QString::fromUtf8(ps->readAllStandardError());
        if (!messages.count() && !result.isEmpty() && exitCode != 0)
            messages << tr("Standard error: ") << result;

        if (messages.count())
            emit openFileRespons(title, messages);

        ps->deleteLater();
    });

    ps->start("/bin/bash", { getFullPath(), host, sendUser, sendPass, port });
}

bool Suspend::isFileExsit()
{
    if(!QFileInfo::exists(getFullPath()) || suspendFile.isEmpty()) return false;
    return true;
}

bool Suspend::selectFile()
{
    QStringList suffix = { "ShellScript(*.sh)", tr("all(*.*)") };

    auto filePath = QFileDialog::getOpenFileName(parent, tr("Open file"),
        scriptPath, suffix.join(";;"), &suffix[0]);

    if (filePath.isEmpty()) return false;
    QFileInfo fileInfo(filePath);
    suspendFile = fileInfo.fileName();
    suspendType = getType(suspendFile);
    return true;
}

SuspendType Suspend::getType(const QString &fileName)
{
    if (fileName.isEmpty()) return SuspendType::none;
    if (fileName == suspendTypeNames[(int)SuspendType::Linux].file) return SuspendType::Linux;
    if (fileName == suspendTypeNames[(int)SuspendType::Windows].file) return SuspendType::Windows;
    return SuspendType::Other;
}

QString Suspend::getFullPath()
{
    return QDir::cleanPath(scriptPath + QDir::separator() + suspendFile);
}
