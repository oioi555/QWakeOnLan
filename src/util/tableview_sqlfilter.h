#ifndef TABLEVIEW_SQLFILTER_H
#define TABLEVIEW_SQLFILTER_H

#include <QTableView>

#include "util/sqlmodel.h"
#include "sortfilterproxymodel.h"
#include "filter_header.h"

class TableViewSqlFilter : public QTableView
{
    Q_OBJECT

private:
    QSqlQueryModel *queryModel;
    SortFilterProxyModel *proxyModel;
    FilterHeader *header;
    SqlModel *sqlModel;
    int selectedId;
    int idColumn;

public:
    explicit TableViewSqlFilter(QWidget *parent = nullptr);
    virtual ~TableViewSqlFilter() override;

    // 初期値セット
    void setSqlModel(SqlModel *sqlModel) {
        this->sqlModel = sqlModel;
    }

    // SQLパラメーターをセットしてから呼び出す
    void initSql();
    void reQuery(bool useLastQuery = true);

    // ヘッダーのラベル設定
    void setHeaderLabels(const QStringList &names);

    // レコード選択
    bool isSelected();
    int getSelectedId();
    bool setSelectId(const int &selectID);
    QList<int> getSelectedIds();
    bool setFilter(const QString &title, QString word, const int &selectID = -1) {
        header->setFilterText(title, word);
        return setSelectId(selectID);
    }
    void resetFiler() {
        header->resetFilter();
    }
    QString getTitleFilters() {return header->getTitleFilters();}

    void autoSizeColRow() {
        this->resizeColumnsToContents();
        //this->resizeRowsToContents();
        header->fixWidgetPositions();
    }
    QSize getContentsSize();

    // ヘッダー設定の保存・復元
    QByteArray saveState() {
        return header->saveState();
    }
    void restoreState(QByteArray aData) {
        header->restoreState(aData);
        header->syncCheckColumns();
    }
    QVariantList getColumnsState() {
        return header->getColumnsState();
    }
    void setColumnsState(const QVariantList &list) {
        header->setColumnsState(list);
    }
signals:
    void selectionChangedId(int id);
    void updateFilterEvent(const QString &filters);
    void headerResized(int logical, int oldSize, int newSize);
    void columnsStateChanged();
    void mousePress(QMouseEvent *event);
    void mouseRelease(QMouseEvent *event);
    void mouseMove(QMouseEvent *event);

public slots:
    void updateFilter();

    // QAbstractItemView interface
protected slots:
    virtual void horizontalScrollbarValueChanged(int value) override;
    virtual void selectionChanged(const QItemSelection &selected, const QItemSelection &deselected) override;

    // QWidget interface
protected:
    virtual void mousePressEvent(QMouseEvent *event) override {
        QTableView::mousePressEvent(event);
        emit mousePress(event);
    }
    virtual void mouseReleaseEvent(QMouseEvent *event) override {
        emit mouseRelease(event);
        QTableView::mouseReleaseEvent(event);
    }
    virtual void mouseMoveEvent(QMouseEvent *event) override {
        emit mouseMove(event);
    }
};

#endif // TABLEVIEW_SQLFILTER_H
