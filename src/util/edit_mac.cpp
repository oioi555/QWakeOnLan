#include "edit_mac.h"

#include <QAction>
#include <QIcon>

EditMac::EditMac(QWidget *parent) : QLineEdit(parent)
{
    auto action = this->addAction(QIcon::fromTheme("view-refresh"), QLineEdit::TrailingPosition);
    action->setToolTip(tr("Auto detect"));
    connect(action, &QAction::triggered, this, &EditMac::actionClicked);
}
