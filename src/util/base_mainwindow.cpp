#include "base_mainwindow.h"

#include <QMenu>
#include <QToolBar>
#include <QStandardPaths>
#include <QMessageBox>

BaseMainWindow::BaseMainWindow(QWidget *parent) : QMainWindow(parent)
    , settings(new Settings())
    , appDataPath(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation))
    , toolBarStyleGrp(new QActionGroup(this))
{
    // action toolbar syle custom
    actionToolBarIconTextV = new QAction(tr("Icons and text (vertical)"));
    actionToolBarIconTextH = new QAction(tr("Icons and text (horizontal)"));
    actionToolBarIcon = new QAction(tr("Icon only"));
    actionToolBarText = new QAction(tr("Text only"));

    QList<toolBarStyleItem> styleList = {
        {Qt::ToolButtonTextUnderIcon, actionToolBarIconTextV},
        {Qt::ToolButtonTextBesideIcon, actionToolBarIconTextH},
        {Qt::ToolButtonIconOnly, actionToolBarIcon},
        {Qt::ToolButtonTextOnly, actionToolBarText}
    };
    foreach (auto item, styleList) {
        item.action->setCheckable(true);
        item.action->setData(item.style);
        toolBarStyleGrp->addAction(item.action);
        connect(item.action, &QAction::triggered, this, [=] {
            setToolBarStyle(item.style);
        });
    }
}

BaseMainWindow::~BaseMainWindow()
{
    delete settings;
    delete toolBarStyleGrp;
    delete actionToolBarIconTextV;
    delete actionToolBarIconTextH;
    delete actionToolBarIcon;
    delete actionToolBarText;
}

/*---------------------------------------------------
 * ToolBar
----------------------------------------------------*/
void BaseMainWindow::setupToolBar(QToolBar *toolBar)
{
    this->toolBar = toolBar;

    uint styleint = settings->value("setToolBarStyle", Qt::ToolButtonTextUnderIcon).toUInt();
    auto selectStyle = static_cast<Qt::ToolButtonStyle>(styleint);
    setToolBarStyle(selectStyle);
    foreach (auto action, toolBarStyleGrp->actions()) {
        if (action->data() == selectStyle) {
            action->setChecked(true);
            break;
        }
    }
}

void BaseMainWindow::setToolBarStyle(Qt::ToolButtonStyle style)
{
    if (style == Qt::ToolButtonTextUnderIcon)
        toolBar->setIconSize(QSize(48, 22));
    else
        toolBar->setIconSize(QSize(22, 22));

    toolBar->setToolButtonStyle(style);
    settings->setValue("setToolBarStyle", style);
}

QMenu *BaseMainWindow::createPopupMenu()
{
    QMenu* menu= QMainWindow::createPopupMenu();
    menu->clear();
    menu->addSection(tr("Toolbar style"));
    menu->addActions(toolBarStyleGrp->actions());
    return menu;
}

/*---------------------------------------------------
 * Other
----------------------------------------------------*/
bool BaseMainWindow::deleteConfirm(QWidget *parent)
{
    QWidget *widget = parent == nullptr? qobject_cast<QWidget*>(this->window()) : parent;
    QMessageBox msgBox(widget);
    msgBox.setWindowTitle(tr("Delete"));
    msgBox.setText(tr("Are you sure you want to delete it?"));
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Cancel);
    msgBox.setIcon(QMessageBox::Question);
    return (msgBox.exec() == QMessageBox::Yes);
}




