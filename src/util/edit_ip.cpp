#include "edit_ip.h"
#include <QApplication>
#include <QAction>
#include <QHostAddress>
#include <QNetworkInterface>
#include <QMessageBox>

EditIP::EditIP(QWidget *parent) : QLineEdit(parent)
  , ipMin(1)
  , ipMax(254)
{
    this->setClearButtonEnabled(true);

    auto actionSearch = this->addAction(QIcon::fromTheme("search"), QLineEdit::TrailingPosition);
    actionSearch->setToolTip(tr("Scan"));
    connect(actionSearch, &QAction::triggered, this, [=] () {
        emit actionSearchClicked();
    });

    auto actionRefresh = this->addAction(QIcon::fromTheme("view-refresh"), QLineEdit::LeadingPosition);
    actionRefresh->setToolTip(tr("Auto detect"));
    connect(actionRefresh, &QAction::triggered, [=] {
        ipMin = 1;
        ipMax = 254;
        setAutoRange();
    });
}

void EditIP::setAutoRange()
{
    QStringList hosts;
    const QHostAddress &localhost = QHostAddress(QHostAddress::LocalHost);
    for (const QHostAddress &address: QNetworkInterface::allAddresses()) {
        if (address.protocol() == QAbstractSocket::IPv4Protocol && address != localhost) {
            hosts = address.toString().split("."); break;
        }
    }
    if (hosts.count() > 3) {
        hosts[3] = QString::number(ipMin);
        for (int i = 0; i < hosts.count(); i++)
            hosts[i] = hosts[i].rightJustified(3, '0');

        setText(hosts.join(".") + "-" + QString::number(ipMax).rightJustified(3, '0'));
    }
}

bool EditIP::parseIP()
{
    if (text().isEmpty()) {
        QMessageBox::critical(this, tr("IP range"), tr("No IP address specified"));
        return false;
    }

    QString max = "";
    QString ip = "";
    QStringList maxList = text().split("-");
    if (maxList.count() > 1) {
        ip = maxList.at(0);
        max = maxList.at(1);
    }
    if (max.isEmpty() || ip.isEmpty()) {
        QMessageBox::critical(this, tr("IP range"), tr("IP address range is invalid"));
        return false;
    }

    ipAddress = ip.split(".");
    ipMin = ipAddress[3].toInt();
    ipMax = max.toInt();
    if (ipMin < ipMax) return true;

    QMessageBox::critical(this, tr("IP range"), tr("The end address should be larger than the start address"));
    return false;
}

void EditIP::showEvent(QShowEvent */*event*/)
{
    this->setInputMask(tr("000.000.000.000-000;_"));
}

