#ifndef EDIT_PATH_FILE_SELECT_H
#define EDIT_PATH_FILE_SELECT_H

#include <QLineEdit>

class EditPathFileSelect : public QLineEdit
{
    Q_OBJECT

public:
    explicit EditPathFileSelect(QWidget *parent = nullptr);

signals:
    void actionClicked();
};

#endif // EDIT_PATH_FILE_SELECT_H
