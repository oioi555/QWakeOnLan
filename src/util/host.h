#ifndef HOST_H
#define HOST_H

#include <QVariant>
#include <QSqlRecord>
#include <QStandardPaths>
class QHostInfo;
class Host
{

public:
    explicit Host(){};

    // from ping interval
    explicit Host(const QSqlRecord &rs, int pingWait) : pingWait(pingWait) {
        host = rs.value("host").toString();
        mac = rs.value("mac").toString();
        ping();
    }

    // from scanner
    explicit Host(int pingWait, const QString &host):
        host(host), pingWait(pingWait), active(false) { loadOuiFilePath(); }

    // from edit
    explicit Host(const QString &host, const QString &name, const QString &mac):
    host(host), name(name), mac(mac){}

    bool ping();
    void scan();
    bool scanMac();
    bool scanVender();
    bool wakeOnLan();

    QString getIp() const { return host; }
    QString getName() const { return name; }
    QString getMac() const { return mac; }
    QString getVendorId();
    QString getVendorName() const { return vendorName; }
    bool isActive() const { return active; }
    void loadOuiFilePath();
    void setName(const QString &name) { this->name = name; }
    void setIp(const QString &ip) { this->host = ip; }

private:
    QString host = "";
    QString name = "";
    QString mac = "";
    QString vendorName = "";

    int pingWait = 50;
    bool active = false;
    QString ouiFilePath = "";
};
#endif // HOST_H
