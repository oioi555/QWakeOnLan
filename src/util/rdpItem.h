#ifndef RDPITEM_H
#define RDPITEM_H

#include <QIcon>
#include <QObject>
#include "host.h"

enum class RdpType{
    None,
    Remmina,
    NoManine,
    Other
};

class QComboBox;
class Settings;
class RdpItem : public QObject
{
    Q_OBJECT

public:
    explicit RdpItem(QWidget *parent = nullptr) : parent(parent) { init(); }
    explicit RdpItem(const QSqlRecord &rs, QWidget *parent = nullptr) : parent(parent) {
        init();
        rdpType = castRdpType(rs.value("rdpType").toInt());
        fileName = rs.value("linkFile").toString();
    }
    ~RdpItem();

    void setValue(int type, const QString fileName) {
        rdpType = castRdpType(type);
        this->fileName = fileName;
    }

    // setup ui
    void setupRdpComboItems(QComboBox* cmb);
    QIcon getIcon(int type, const QString fileName) {
        setValue(type, fileName);
        return getIcon();
    }
    QIcon getIcon();

    // rdp file
    bool opneRdpFile();
    bool isFileExsit();
    bool selectRdpFile(int type);
    bool readRdpFile(Host &host);
    QString getRdpPath(RdpType type);
    QString getFileName() { return fileName; }
    RdpType castRdpType(int type) { return static_cast<RdpType>(type); }
    RdpType getRdpType() { return rdpType; }
    int getRdpTypeIndex() { return (int)rdpType; }

private:
    void init();

    QString getRdpFullPath();
    bool readNsx(Host &host);
    bool readRemmina(Host &host);

    RdpType rdpType = RdpType::Other;
    QString fileName = "";

    struct RdpName {
        QString name;
        QIcon icon;
    };
    QList<RdpName> rdpTypeNames;
    QWidget *parent = nullptr;
    Settings *settings;
};

#endif // RDPITEM_H
