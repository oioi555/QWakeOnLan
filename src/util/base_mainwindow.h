#ifndef BASE_MAINWINDOW_H
#define BASE_MAINWINDOW_H

#include <QMainWindow>
#include "util/settings.h"

class QMdiArea;
class QMdiSubWindow;
class QAction;
class QActionGroup;
class QToolBar;

class BaseMainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit BaseMainWindow(QWidget *parent = nullptr);
    virtual ~BaseMainWindow() override;

    // settings helper
    Settings* getSettings() {return settings;}
    bool deleteConfirm(QWidget *parent = nullptr);

protected:
    Settings *settings;
    QString appDataPath;

    // toolBar
    void setupToolBar(QToolBar *toolBar);

private:
    // toolBar
    void setToolBarStyle(Qt::ToolButtonStyle style);
    QToolBar *toolBar;
    QAction *actionToolBarIconTextV;
    QAction *actionToolBarIconTextH;
    QAction *actionToolBarIcon;
    QAction *actionToolBarText;
    struct toolBarStyleItem  {
        Qt::ToolButtonStyle style;
        QAction* action;
    };
    QActionGroup *toolBarStyleGrp;

signals:

    // QMainWindow interface
public:
    virtual QMenu *createPopupMenu() override;
};

#endif // BASE_MAINWINDOW_H
