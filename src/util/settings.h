#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QSettings>
#include <QHash>
#include <QStandardPaths>
#include <QDir>


class Settings : public QSettings
{
    Q_OBJECT

public:
    explicit Settings() {
        defaultValues = {
            {"pingInterval", "1000"},
            {"pingWaitTime", "50"},
            {"wakeTimeout", "15"},
            {"opneLink", true},
            {"chkDarkIcon", false},
            {"style", "Breeze"},
            {"rdpType", 0},
            {"suspendType", 0},
            {"loginSave", false},
            {"editPathNomachine", QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation) + "/NoMachine" },
            {"editPathRemmina", QStandardPaths::writableLocation(QStandardPaths::HomeLocation) + "/.remmina" },
            {"editPathOther", QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/other" },
            {"editPathSuspend", QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/suspend"},
            {"editOuiFile", QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/oui.txt"},
            {"cmbOuiUrl", "http://standards-oui.ieee.org/oui/oui.txt"},
            {"chkOuiCompleted", false},
            {"chkVersionCheck", true}
        };
        makePathKeys << "editPathOther" << "editPathSuspend";
    }
    ~Settings() {}

    QVariant value(const QString &key) const {
        QString defalutKey = group().isEmpty()? key : group() + "/" + key;
        return QSettings::value(key, defaultValues[defalutKey]);
    }
    QVariant value(const QString &key, const QVariant &defaultValue) const {
        return QSettings::value(key, defaultValue);
    }

    QVariant valueObj(QObject *object, const QString &key) const {
        return value(tr("%1/%2").arg(object->metaObject()->className(), key));
    }
    QVariant valueObj(QObject *object, const QString &key, const QVariant &defaultValue) const {
        return QSettings::value(tr("%1/%2").arg(object->metaObject()->className(), key), defaultValue);
    }

    void setValueObj(QObject *object, const QString &key, const QVariant &value) {
        setValue(tr("%1/%2").arg(object->metaObject()->className(), key), value);
    }
    void beginGroupObj(QObject *object) {
        beginGroup(object->metaObject()->className());
    }

    void makePaths() {
        foreach (auto key, makePathKeys) makePath(key);
    }
    void makePath(const QString &key) {
        QDir dir(value(key).toString());
        if (!dir.mkpath(dir.absolutePath())) {
            auto defPath = defaultValues[key].toString();
            if (value(key).toString() != defPath) {
                if (dir.mkpath(defPath)) setValue(key, defPath);
            }
        }
    }
    QHash<QString, QVariant> defaultValues;
    QStringList makePathKeys;
};

#endif // SETTINGS_H
