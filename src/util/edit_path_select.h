#ifndef EDIT_PATH_SELECT_H
#define EDIT_PATH_SELECT_H

#include "edit_path.h"

class EditPathSelect : public EditPath
{
    Q_OBJECT

public:
    EditPathSelect(QWidget *parent = nullptr);

private slots:
    void on_actionSelect_clicked();
};

#endif // EDIT_PATH_SELECT_H
