#include "filter_header.h"

#include <QApplication>
#include <QLineEdit>
#include <QContextMenuEvent>
#include <QDebug>

static const int filterReactionTime = 200; // in millisecons
static const int filterBoxSpace = 2;

FilterHeader::FilterHeader(QWidget *parent)
    : QHeaderView(Qt::Horizontal, parent), _menu(tr("Show or hide columns"), this)
{
    setSectionsClickable(true);
    setSectionsMovable(true);
    setDefaultAlignment(Qt::AlignTop | Qt::AlignHCenter);
    setContextMenuPolicy(Qt::DefaultContextMenu);

    connect(this, &QHeaderView::sectionResized,this, &FilterHeader::handleSectionResized);
    connect(this, &QHeaderView::sectionMoved,this, &FilterHeader::handleSectionMoved);
    _filterTimer.setSingleShot(true);
    connect(&_filterTimer, &QTimer::timeout,this, &FilterHeader::triggerFilter);
}

FilterHeader::~FilterHeader()
{
    qDeleteAll(_columnActions);
    qDeleteAll(_filters);
}

// フィルターボックスの位置修正
void FilterHeader::fixWidgetPositions()
{
    for (int i = 0; i < count(); ++i)
        setFilterGeometry(i);
    // 再表示も一緒に行う
    showEvent(nullptr);
}

// 動的フィルター
void FilterHeader::setFilterText(int column, const QString& word)
{
    auto *edit = qobject_cast<QLineEdit*>(_filters[column]);
    if (edit) {
        edit->setText(word);
        emit stringFilterChanged(column, edit->text());
    }
    fixWidgetPositions();
}

void FilterHeader::resetFilter()
{
    bool isChange = false;
    foreach (auto obj, _filters) {
        auto *edit = qobject_cast<QLineEdit*>(obj);
        if (edit) {
            if (!edit->text().isEmpty()) {
                edit->setText("");
                isChange = true;
            }
        }
    }
    if (isChange) emit stringFilterChanged(0, ""); // ダミー
    fixWidgetPositions();
}

// restoreState後にcheck状態を合わせる
void FilterHeader::syncCheckColumns()
{
    for (int i = 0 ; i < count() ; i++) {
        _columnActions[i]->setChecked((!isSectionHidden(i)));
    }
}

// Columnの表示状態を取得
auto FilterHeader::getColumnsState() -> QVariantList
{
    QVariantList result;
    for (int i = 0 ; i < count() ; i++)
        result << (!isSectionHidden(i));
    return result;
}

// Columnの表示状態を設定
void FilterHeader::setColumnsState(const QVariantList &list)
{
    for (int i = 0 ; i < count() ; i++) {
        _columnActions[i]->setChecked(list[i].toBool());
        showOrHideColumn(i);
    }
}

auto FilterHeader::getTitleFilters() -> QString
{
    QStringList result;
    for (int i = 0 ; i < count() ; i++) {
        auto *edit = qobject_cast<QLineEdit*>(_filters[i]);
        if (edit) {
            if (!edit->text().isEmpty())
                result << edit->text();
        }
    }
    return  result.join("｜");
}

QSize FilterHeader::getHeaderSize()
{
    int width = 0;
    for (int i = 0 ; i < count() ; i++) {
        if (_columnActions[i]->isChecked())
            width += sectionSize(i);
    }
    return QSize(width, height());
}

void FilterHeader::contextMenuEvent(QContextMenuEvent* event)
{
    _menu.popup(event->globalPos());
}

void FilterHeader::showEvent(QShowEvent *e)
{
    QFont font;
    QFontMetrics fm(font);
    int margin = QApplication::style()->pixelMetric(QStyle::PM_ButtonMargin);

    int filterHeight = 0;
    for (int i = 0; i < count(); ++i) {
        auto title = model()->headerData(i, orientation()).toString();
        auto rect = fm.boundingRect(QRect(0,0,0,0), Qt::AlignLeft|Qt::AlignTop, title);
        _textHeight = rect.height() + (margin * 2);

        if (!_filters[i]) {
            createFilter(i);
            createAction(i);
        }
        filterHeight = _filters[i]->height();
    }
    setFixedHeight(_textHeight + filterHeight);

    // Finally show each filter
    for (int i = 0; i < count(); ++i) {
        setFilterGeometry(i);
        _filters[i]->show();
    }
    QHeaderView::showEvent(e);
}

void FilterHeader::createFilter(int column)
{
    auto *edit = new QLineEdit(this);
    edit->setPlaceholderText(tr("Filter"));
    edit->setToolTip(tr("Wildcards *, ? oder [] erlaubt. Zum Freistellen \\ vor die Wildcard setzen"));
    edit->setClearButtonEnabled(true);
    connect(edit,&QLineEdit::textChanged, this, [=] {
        filterChanged(column);
    });
    _filters[column] = edit;
}

void FilterHeader::createAction(int column)
{
    QString title = model()->headerData(column, orientation()).toString();
    auto *action = new QAction(title.simplified(), &_menu);
    action->setCheckable(true);
    action->setChecked(!isSectionHidden(column));

    connect(action,&QAction::triggered, this, [=] {
        showOrHideColumn(column);
        emit columnsStateChanged();
    });
    _columnActions.insert(column, action);
    _menu.addAction(action);
}

void FilterHeader::handleSectionResized(int logical, int oldSize, int newSize)
{
    for (int i = visualIndex(logical); i < count(); ++i)
        setFilterGeometry(logicalIndex(i));
    emit headerResized(logical, oldSize, newSize);
}

void FilterHeader::handleSectionMoved(int /*logical*/, int oldVisualIndex, int newVisualIndex)
{
    for (int i = qMin(oldVisualIndex, newVisualIndex); i < count(); ++i)
        setFilterGeometry(logicalIndex(i));
}

void FilterHeader::setFilterGeometry(int column)
{
    if (_filters[column]) {
        _filters[column]->setGeometry(
            sectionViewportPosition(column) + filterBoxSpace, _textHeight -filterBoxSpace,
            sectionSize(column) -(filterBoxSpace * 2), _filters[column]->height());
    }
}

void FilterHeader::showOrHideColumn(int column)
{
    if (!_columnActions.contains(column))
        return;

    QAction* action = _columnActions[column];
    if (action->isChecked()) {
        showSection(column);
    } else {
        // If the user hides every column then the table will disappear. This
        // guards against that. NB: hiddenCount reflects checked QAction's so
        // size-hiddenCount will be zero the moment they uncheck the last
        // section
        if (_columnActions.size() - hiddenCount() > 0) {
            hideSection(column);
        } else {
            // Otherwise, ignore the request and re-check this QAction
            action->setChecked(true);
        }
    }
}

auto FilterHeader::hiddenCount() -> int
{
    int count = 0;
    for (auto action : _columnActions) {
        if (!action->isChecked())
            ++count;
    }
    return count;
}

auto FilterHeader::getColumnNameIndex(const QString &name) -> int
{
    for (int i = 0; i < count(); ++i) {
        if (name == model()->headerData(i, orientation()).toString())
            return i;
    }
    return -1;
}

void FilterHeader::filterChanged(int column)
{
    if (_filterTimer.isActive() && _currentColumn != column) {
        // Let current column complete it's filtering first of all
        _filterTimer.stop();
        triggerFilter();
    }
    _currentColumn = column;
    _filterTimer.start(filterReactionTime);
}

void FilterHeader::triggerFilter()
{
    auto *edit = qobject_cast<QLineEdit*>(_filters[_currentColumn]);
    if (edit)
        emit stringFilterChanged(_currentColumn, edit->text());
}

void FilterHeader::doItemsLayout()
{
    // レイアウトが崩れるので強制再表示
    QHeaderView::doItemsLayout();
    fixWidgetPositions();
}
