#include "rdpItem.h"
#include "settings.h"

#include <QComboBox>
#include <QDir>
#include <QDomDocument>
#include <QFileDialog>
#include <QMessageBox>
#include <QProcess>
#include <QTextCodec>

void RdpItem::init()
{
    settings = new Settings;
    rdpTypeNames = {
        {"None", QIcon::fromTheme("dialog-cancel")},
        {"Remmina", QIcon(":/images/org.remmina.Remmina.png")},
        {"NoManine", QIcon(":/images/NoMachine-icon.png")},
        {"Other", QIcon::fromTheme("akonadiconsole")}
    };
}

RdpItem::~RdpItem()
{
    delete settings;
}

void RdpItem::setupRdpComboItems(QComboBox *cmb)
{
    cmb->clear();
    for (int i = 0; i < rdpTypeNames.count(); i++)
        cmb->insertItem(i, rdpTypeNames[i].icon, rdpTypeNames[i].name);
}

QIcon RdpItem::getIcon()
{
    if (!isFileExsit()) return QIcon::fromTheme("error");
    return rdpTypeNames[(int)rdpType].icon;
}

bool RdpItem::opneRdpFile()
{
    if (!isFileExsit()) {
        QMessageBox::critical(parent, tr("Open the link"), tr("File not found") + "\n" + fileName);
        return false;
    }
    return QProcess::startDetached("xdg-open", {getRdpFullPath()});
}

bool RdpItem::isFileExsit()
{
    if(!QFileInfo::exists(getRdpFullPath()) || fileName.isEmpty()) return false;
    return true;
}

bool RdpItem::selectRdpFile(int type)
{
    if (castRdpType(type) == RdpType::None) return false;

    QStringList suffix = { "", "Remmina(*.remmina)", "NoMachine(*.nxs)", tr("all(*.*)") };

    auto filePath = QFileDialog::getOpenFileName(parent, tr("Open file"),
        getRdpPath(castRdpType(type)), suffix.join(";;"), &suffix[type]);

    if (filePath.isEmpty()) return false;
    QFileInfo fileInfo(filePath);
    fileName = fileInfo.fileName();

    auto ext = fileInfo.suffix().toLower();
    if (ext == "nxs") rdpType = RdpType::NoManine;
    else if (ext == "remmina") rdpType = RdpType::Remmina;
    else rdpType = RdpType::Other;

    return true;
}

bool RdpItem::readRdpFile(Host &host)
{
    if (rdpType == RdpType::Other) return false;

    // Read basic information from RDP file
    QMessageBox msgBox(parent);
    msgBox.setWindowTitle(tr("Import setting value"));
    msgBox.setText(tr("Do you want to import the settings from the RDP file?"));
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Cancel);
    msgBox.setIcon(QMessageBox::Question);
    if (msgBox.exec() != QMessageBox::Yes) return false;

    switch (rdpType) {
    case RdpType::NoManine: return readNsx(host);
    case RdpType::Remmina: return readRemmina(host);
    default:;
    }

    return false;
}

bool RdpItem::readNsx(Host &host)
{
    if (!isFileExsit()) return false;

    auto filePath = getRdpFullPath();
    QFileInfo fileInfo(filePath);
    auto suffix = fileInfo.suffix();
    auto name = fileInfo.fileName();
    name.replace("." + suffix, "");
    host.setName(name);

    QFile file(filePath);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return false;

    QDomDocument xml;
    if (!xml.setContent(&file)) {
        file.close(); return false;
    }
    file.close();

    auto root = xml.documentElement();
    auto group = root.firstChild().toElement();
    while(!group.isNull()) {
        if (group.tagName() == "group") {
            if (group.attribute("name", "") == "General") {
                auto Child = group.firstChild().toElement();
                while (!Child.isNull()) {
                    if (Child.tagName() == "option") {
                        if (Child.attribute("key", "") == "Server host") {
                            host.setIp(Child.attribute("value", ""));
                            return true;
                        }
                    }
                    Child = Child.nextSibling().toElement();
                }
            }
        }
        group = group.nextSibling().toElement();
    }
    return false;
}

bool RdpItem::readRemmina(Host &host)
{
    if (!isFileExsit()) return false;

    QSettings settings(getRdpFullPath(), QSettings::IniFormat);
    settings.setIniCodec(QTextCodec::codecForName("UTF-8"));
    host.setName(settings.value("remmina/name").toString());
    host.setIp(settings.value("remmina/server").toString());
    return true;
}

QString RdpItem::getRdpFullPath()
{
    return QDir::cleanPath(getRdpPath(rdpType) + QDir::separator() + fileName);
}

QString RdpItem::getRdpPath(RdpType type)
{
    QString key;
    switch (type) {
    case RdpType::None: key = "editPathOther"; break;
    case RdpType::Remmina: key = "editPathRemmina"; break;
    case RdpType::NoManine: key = "editPathNomachine"; break;
    case RdpType::Other: key = "editPathOther"; break;
    default:{}
    }
    return settings->value(key).toString();
}
