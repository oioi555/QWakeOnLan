#ifndef FILTERHEADER_H
#define FILTERHEADER_H

#include <QHeaderView>
#include <QMenu>
#include <QTimer>

class FilterHeader : public QHeaderView
{
	Q_OBJECT

public:
    explicit FilterHeader(QWidget *parent = nullptr);
    ~FilterHeader() override;

	void fixWidgetPositions();
    void setFilterText(int column , const QString& word);
    void setFilterText(const QString &name, QString word) {
        int column = getColumnNameIndex(name);
        if (column != -1) setFilterText(column, word);
    }
    void resetFilter();
    void syncCheckColumns();// restoreState後にcheck状態を合わせる
    QVariantList getColumnsState();
    void setColumnsState(const QVariantList &list);
    QString getTitleFilters();
    QSize getHeaderSize();

signals:
	void stringFilterChanged(int,QString);
	void stateFilterChanged(int,Qt::CheckState);
    void headerResized(int logical, int oldSize, int newSize);
    void columnsStateChanged();

private slots:
    void handleSectionResized(int logical, int oldSize, int newSize);
	void handleSectionMoved(int logical, int oldVisualIndex, int newVisualIndex);
    void showOrHideColumn(int);
	void filterChanged(int);
	void triggerFilter();

protected:
	virtual void showEvent(QShowEvent *e) override;
	virtual void contextMenuEvent(QContextMenuEvent* event) override;

private:
	void setFilterGeometry(int column);
	void createFilter(int column);
	void createAction(int column);
	int hiddenCount();
    int getColumnNameIndex(const QString &name);
	QMap<int, QAction*> _columnActions;
	QMap<int, QWidget*> _filters;
	QMenu _menu;
	QTimer _filterTimer;
	int _currentColumn{};
	int _textHeight{};

    // QAbstractItemView interface
public slots:
    virtual void doItemsLayout() override;
};

#endif // FILTERHEADER_H
