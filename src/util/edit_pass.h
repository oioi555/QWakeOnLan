#ifndef EDIT_PASS_H
#define EDIT_PASS_H

#include <QLineEdit>

class EditPass : public QLineEdit
{
    Q_OBJECT
public:
    explicit EditPass(QWidget *parent = nullptr);
};

#endif // EDIT_PASS_H
