#include "edit_path_file_select.h"
#include <QAction>

EditPathFileSelect::EditPathFileSelect(QWidget *parent) : QLineEdit(parent)
{
    auto action = this->addAction(QIcon::fromTheme("document-edit"), QLineEdit::TrailingPosition);
    action->setToolTip(tr("Select file"));
    connect(action, &QAction::triggered, this, &EditPathFileSelect::actionClicked);
}
