#ifndef SUSPEND_H
#define SUSPEND_H

#include <QObject>
#include <QProcess>
#include <QSqlRecord>
#include <QSystemTrayIcon>
#include <QVariant>

enum class SuspendType{
    none,
    Linux,
    Windows,
    Other
};

class QComboBox;
class Settings;
class PopUp;

class Suspend : public QObject
{
    Q_OBJECT

public:
    explicit Suspend(QWidget *parent = nullptr);
    ~ Suspend();

    // read
    void setValue(const QSqlRecord &rs) {
        suspendType = castType(rs.value("suspendType").toInt());
        suspendFile = rs.value("suspendFile").toString();
        name = rs.value("name").toString();
        host = rs.value("host").toString();
        user = rs.value("user").toString();
        pass = rs.value("pass").toString();
        port = rs.value("port").toString();
    }

    // setup ui
    void setupScriptFiles();
    void setupComboItems(QComboBox* cmb);
    QIcon getIcon(int type, const QString fileName);

    // login
    bool isCheckLoginInfo();

    // Encrypt
    QString getClearPass(const QString &decryptedPass);
    QString getDecryptedPass(const QString &clearPass);

    // file
    void opneFile(const QString &user = "", const QString &pass = "");
    bool isFileExsit();
    bool selectFile();
    QString getScriptPath() { return scriptPath; }
    QString getFileName() { return suspendFile; }
    QString getFileName(int type) { return suspendTypeNames[type].file; }
    SuspendType castType(int type) { return static_cast<SuspendType>(type); }
    int getTypeIndex() { return (int)suspendType; }
    SuspendType getType() { return suspendType; }
    SuspendType getType(const QString &fileName);

signals:
    void openFileRespons(const QString &title, const QStringList &messages,
        QSystemTrayIcon::MessageIcon icon = QSystemTrayIcon::Critical);

private:
    QString getFullPath();
    QString scriptPath;
    SuspendType suspendType;
    struct SuspendName {
        QString name;
        QIcon icon;
        QString file;
    };
    QList<SuspendName> suspendTypeNames;
    QString suspendFile = "";
    QString name = "";
    QString host = "";
    QString user = "";
    QString pass = "";
    QString port = "";

    QHash<int, QString> exitCodeMessages;
    QHash<QProcess::ProcessError, QString> processErrors;

    QWidget *parent = nullptr;
    Settings *settings;
};

#endif // SUSPEND_H
