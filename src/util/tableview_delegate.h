#ifndef TABLEVIEW_DELEGATE_H
#define TABLEVIEW_DELEGATE_H

#include "rdpItem.h"
#include "sqlmodel.h"
#include "suspend.h"

#include <QStyledItemDelegate>
#include <QApplication>
#include <QDebug>
#include <QPainter>

class TableViewDelegate : public QStyledItemDelegate
{
    Q_OBJECT

public:
    explicit TableViewDelegate(QObject *parent = nullptr): QStyledItemDelegate(parent) {
        szIcon = QSize(22, 22);
        margin = QApplication::style()->pixelMetric(QStyle::PM_DefaultLayoutSpacing);

        sqlModel = new SqlModel("device", "id");
        rdp = new RdpItem;
        suspend = new Suspend;
    }

    ~TableViewDelegate(){
        delete sqlModel;
        delete rdp;
        delete suspend;
    }

private:
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
    {
        QStyleOptionViewItem optCopy = option;

        // status
        if (index.column() == 1) {
            auto status = index.data(Qt::DisplayRole).toBool();
            drawIcon(QIcon::fromTheme(status? "online" : "offline"), painter, optCopy);
            return;
        }

        // rdp type file
        if (index.column() == 5) {
            auto fileName = index.data(Qt::DisplayRole).toString();
            auto icon = rdp->getIcon(getRecordValue("rdpType", index).toInt(), fileName);
            drawIconLabel(icon, fileName, painter, optCopy);
            return;
        }

        // suspend type file
        if (index.column() == 6) {
            auto fileName = index.data(Qt::DisplayRole).toString();
            auto icon = suspend->getIcon(getRecordValue("suspendType", index).toInt(), fileName);
            drawIconLabel(icon, fileName, painter, optCopy);
            return;
        }

        QStyledItemDelegate::paint(painter, optCopy, index);
    }

    void drawIconLabel(const QIcon &icon, const QString &text, QPainter *painter, QStyleOptionViewItem &option) const {
        painter->save();

        // Apply selected background
        option.widget->style()->drawControl(QStyle::CE_ItemViewItem, &option, painter);

        auto pixmap = icon.pixmap(icon.actualSize(szIcon));
        QRect rcPixmap = option.rect.adjusted(margin, 0, - (option.rect.width() - (pixmap.rect().width() + margin)), 0);
        QRect rcText = option.rect.adjusted(pixmap.rect().width() + (margin * 2), 0, -margin, 0);

        option.displayAlignment = Qt::AlignLeft | Qt::AlignVCenter;
        auto style = QApplication::style();
        style->drawItemPixmap(painter, rcPixmap, option.displayAlignment , pixmap);
        style->drawItemText(painter, rcText, option.displayAlignment, QApplication::palette(), true, text);

        painter->restore();
    }

    void drawIcon(const QIcon &icon, QPainter *painter, QStyleOptionViewItem &option) const {
        painter->save();

        // Apply selected background
        option.widget->style()->drawControl(QStyle::CE_ItemViewItem, &option, painter);

        auto pixmap = icon.pixmap(icon.actualSize(szIcon));
        option.displayAlignment = Qt::AlignHCenter | Qt::AlignVCenter;
        QApplication::style()->drawItemPixmap(painter, option.rect, option.displayAlignment , pixmap);

        painter->restore();
    }

    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const {
        QSize size = QStyledItemDelegate::sizeHint(option, index);
        switch (index.column()) {
        case 1: size = szIcon; break; // icon
        case 5: // iconLabel
        case 6: size.setWidth(size.width() + szIcon.width() + margin); break;
        }
        return size;
    }

    QVariant getRecordValue(const QString &key, const QModelIndex &index) const {
        auto model = index.model();
        auto id = model->data(model->index(index.row(), 0, QModelIndex()), Qt::DisplayRole).toInt();
        return sqlModel->getValue(key, id);
    }

    QSize szIcon;
    int margin;
    SqlModel *sqlModel;
    RdpItem *rdp;
    Suspend *suspend;

};
#endif // TABLEVIEW_DELEGATE_H
