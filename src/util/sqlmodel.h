﻿#ifndef SQL_MODEL_H
#define SQL_MODEL_H

#include <QObject>
#include <QDate>
#include <QSqlError>
#include <QSqlQueryModel>
#include <QSqlDatabase>
#include <QSqlField>
#include <QSqlDriver>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QFile>

enum class OrderType {asc, desc};    // Sortの昇順降順指定
enum class LimitType {limitAll , limutTop , limitLast}; // リミット指定
enum class SelectType{
    select,
    selectWhere,
    lastSQL
};

class SqlModel : public QObject
{
    Q_OBJECT

private:
    QSqlQueryModel *queryModel;
    QString tableName;
    QString viewName;
    QString idName;
    QString select;
    QString where;
    QString orderBy;
    QString group;
    LimitType limitTypeData;
    int limitNumber;
    QString sqlLast;
    QHash <QString, QString> fieldItems;
    QString getFrom();

public:
    explicit SqlModel(): queryModel(new QSqlQueryModel) {}
    explicit SqlModel(const QString &tableName ,
        const QString &idName ,
        const QString &viewName = "",
        const QString &select = "*" ,
        const QString &where = "" ,
        const QString &orderBy = "",
        const QString &group = ""): queryModel(new QSqlQueryModel)
    {
        setInit(tableName, idName, viewName, select, where, orderBy, group);
    }
    ~SqlModel() {delete queryModel;}

    void setInit(const QString &tableName ,
        const QString &idName ,
        const QString &viewName = "",
        const QString &select = "*" ,
        const QString &where = "" ,
        const QString &orderBy = "",
        const QString &group = "")
    {
        this->viewName = viewName;
        this->tableName = tableName;
        this->idName = idName;
        this->select = select;
        this->where = where;
        this->orderBy = orderBy;
        this->group = group;
        this->limitTypeData = LimitType::limitAll;
    }

    void setWhere(const QString &where) {this->where = where;}
    void setLimit(LimitType limitType , int limit = 100) {
        this->limitNumber = limit; this->limitTypeData = limitType;
    }
    void setOrderBy(const QString &orderBy) {this->orderBy = orderBy;}

    QString getSqlSelectWhere(bool bOrderBy = false);
    QString getSqlSelect(bool bOrderBy = false);
    QString getSqlDelete(const QList<int> &ids);
    QString getSqlDelete(const int id) {
        QList<int> ids = {id};
        return getSqlDelete(ids);
    }
    QString getSqlDelete(const QString &where);
    QString getSqlFilterGroup(const QString &sFieldName ,const OrderType order= OrderType::asc , bool useWhere=false);
    QString getSqlLast() const {return sqlLast;}
    QString getIdName() const {return idName;}

    bool hasView() {return !viewName.isEmpty();}
    bool isOrderBy() {return (!orderBy.isEmpty());}
    bool isWhere() {return !where.isEmpty();}

    // レコード情報取得
    QSqlQueryModel* getModel(SelectType type = SelectType::selectWhere);
    QSqlQueryModel* getModel(const QString &sql);
    QSqlRecord getRecord(const int id);
    QVariant getValue(const QString &sKey, const int id) {
        return getRecord(id).value(sKey);
    }
    QStringList getValues(const QString &fieldName, const QString &where = "", const QString &order = "");
    QList<int> getValuesId(const QString &where = "", const QString &order = "");

    // カウント
    int getRecordCount();
    int getRecordCountWhere(const QString &where = "");

    // 補助
    QStringList getFieldNames();
    QStringList getGroupByValues(const QString &fieldName, const OrderType order = OrderType::asc, bool useWhere = false) {
        return getOneFieldValues(getSqlFilterGroup(fieldName , order , useWhere));
    }

    // 削除
    bool deleteRecord(const int id) {
        return execSql(getSqlDelete(id));
    }
    bool deleteRecord(const QList<int> &ids) {
        return execSql(getSqlDelete(ids));
    }
    bool deleteRecord(const QString &where) {
        return execSql(getSqlDelete(where));
    }

    /*----------------------------------------
     * Update、Insert用の補助コマンド
     * ---------------------------------------*/
    void setValueClear() {fieldItems.clear();}
    void setValue(const QString &sKey, const QString &sValue) {
        auto value = sValue;
        value.replace("'", "");
        fieldItems[sKey] = tr("'%1'").arg(value);
    }
    void setValue(const QString &sKey, const int nValue, bool zeroToNull = false) {
        fieldItems[sKey] = (zeroToNull && nValue == 0)? "null" : QString::number(nValue);
    }
    void setValue(const QString &sKey, const double dValue, const int digit = 1) {
        fieldItems[sKey] = QString::number(dValue , 'f', digit);
    }
    void setValue(const QString &sKey, const QDate tValue, const QString & sFormat = "yyyy-MM-dd") {
        fieldItems[sKey] = tr("'%1'").arg(tValue.toString(sFormat));
    }
    void setValue(const QString &sKey, const QDateTime tValue, const QString & sFormat = "yyyy-MM-dd hh:mm:ss") {
        fieldItems[sKey] = tr("'%1'").arg(tValue.toString(sFormat));
    }
    void setValue(const QString &sKey, const QTime tValue) {
        fieldItems[sKey] = tr("'%1'").arg(tValue.toString());
    }
    void setValue(const QString &sKey, bool bValue) {
        fieldItems[sKey] = bValue? "true" : "false";
    }
    void setValue(const QString &sKey) {
        fieldItems[sKey] = "null";
    }
    // posgreからmysqlにインポートできないので、未使用
    // timestanpの出力が、mysqlで読めない
    void setValue(const QSqlField &field, bool setBackquote = true) {
        QString key = setBackquote? tr("`%1`").arg(field.name()) : field.name();
        fieldItems[key] = QSqlDatabase::database().driver()->formatValue(field);
    }

    void setValue(const QString &sKey, const QVariant &vValue) {
        switch (static_cast<int>(vValue.type())) {
        case QVariant::Invalid:     { setValue(sKey); break;}
        case QVariant::Bool:        { setValue(sKey, vValue.toBool()); break;}
        case QVariant::Int:         { setValue(sKey, vValue.toInt()); break; }
        case QVariant::UInt:        { setValue(sKey, vValue.toInt()); break; }
        case QVariant::ULongLong:   { setValue(sKey, vValue.toInt()); break; }
        case QVariant::Double:      { setValue(sKey, vValue.toDouble()); break; }
        case QVariant::Date:        { setValue(sKey, vValue.toDate()); break; }
        case QVariant::DateTime:    { setValue(sKey, vValue.toDateTime()); break; }
        case QVariant::Time:        { setValue(sKey, vValue.toTime()); break; }
        case QVariant::String:
        default: setValue(sKey, vValue.toString());
        }
    }

    QString getSqlUpdateFieldsValues(const int id = -1);
    QString getSqlInsertFieldsValues(const int id = -1);

    int saveValues(int id = -1) ;
    int getInsertedId() ;

    /*----------------------------------------
     * 汎用
     * ---------------------------------------*/
    bool execSql(const QString &sql);
    bool execSqlFile(const QString &filePath);
    QVariant getScholar(const QString &sql);
    QStringList getOneFieldValues(const QString &sql);
    void deleteTable(const QString &table = "");
    QStringList getSqlTableDump(const QString &table = ""); // 汎用エクスポート
    QString getDriverName() const {return QSqlDatabase::database().driverName();}
    bool isPgsql() { return getDriverName() == "QPSQL"; }
    bool isMysql() { return getDriverName() == "QMYSQL"; }
    bool isSQLite() {return getDriverName() == "QSQLITE"; }

signals:
    void sqlError(const QString &sql , const QSqlError &lastError);
};

#endif // SQL_MODEL_H
