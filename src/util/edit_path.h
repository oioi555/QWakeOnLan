#ifndef EDIT_PATH_H
#define EDIT_PATH_H

#include <QLineEdit>

class EditPath : public QLineEdit
{
    Q_OBJECT

public:
    explicit EditPath(QWidget *parent = nullptr);

};

#endif // EDIT_PATH_H
