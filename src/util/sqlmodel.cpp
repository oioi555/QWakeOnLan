﻿#include "sqlmodel.h"
#include <QDebug>
#include <QSqlTableModel>

auto SqlModel::getFrom() -> QString
{
    select = select.isEmpty() ? "*" : select;
    return hasView()? viewName : tableName;
}

auto SqlModel::getSqlSelectWhere(bool bOrderBy) -> QString
{
    QString sql;
    if (group.isEmpty()) {
        sql = getSqlSelect(false);
        if (!where.isEmpty())
            sql += " WHERE " + where;
        if (!orderBy.isEmpty() && bOrderBy)
            sql += " ORDER BY " + orderBy;
    } else {
        // グループ指定時
        sql = tr("SELECT %1 FROM %2 ").arg(group , getFrom());
        if (!where.isEmpty())
            sql += " WHERE " + where;
        sql = tr("%1 GROUP BY %2").arg(sql, group);
    }

    // Limit指定
    if (this->limitTypeData == LimitType::limutTop) {
        sql = tr("%1 LIMIT %2 OFFSET 0").arg(sql).arg(limitNumber);
    } else if (this->limitTypeData == LimitType::limitLast) {
        // レコードカウント取得
        int count = getRecordCount();
        int offset = (count-limitNumber>0)?count-limitNumber:0;
        sql = tr("%1 LIMIT %2 OFFSET %3").
                arg(sql).arg(limitNumber).arg(offset);
    }
    sqlLast = sql;
    return sql;
}

auto SqlModel::getSqlSelect(bool bOrderBy) -> QString
{
    QString sql;
    sql = tr("SELECT %1 FROM %2 ").arg(select, getFrom());
    if (!orderBy.isEmpty() && bOrderBy)
        sql += " ORDER BY " + orderBy;

    sqlLast = sql;
    return sql;
}

auto SqlModel::getSqlDelete(const QList<int> &ids) -> QString
{
    QString sql;
    QStringList idsStr;
    foreach (auto id, ids)
        idsStr << QString::number(id);
    sql = tr("DELETE FROM %1 WHERE %2 in (%3);").arg(tableName, idName).arg(idsStr.join(","));
    return sql;
}

auto SqlModel::getSqlDelete(const QString &where) -> QString
{
    QString sql;
    sql = tr("DELETE FROM %1 WHERE %2;").arg(tableName,where);
    return sql;
}

auto SqlModel::getModel(SelectType type) -> QSqlQueryModel *
{
    QString sql;
    switch (type) {
    case SelectType::select:
        sql = getSqlSelect(isOrderBy());
        break;
    case SelectType::selectWhere:
        sql = getSqlSelectWhere(isOrderBy());
        break;
    case SelectType::lastSQL:
        sql = getSqlLast();
        break;
    }

    return getModel(sql);
}

auto SqlModel::getSqlFilterGroup(const QString &sFieldName, OrderType order ,bool bColumnFilter) -> QString
{
    QString sql;
    sql = tr("SELECT %1 FROM %2 ").arg( sFieldName, getFrom());
    if (!where.isEmpty() && bColumnFilter) sql += " WHERE " + where;

    sql = tr("%1 GROUP BY %2 ORDER BY %2 %3").
            arg( sql, sFieldName , (order==OrderType::asc)?"ASC":"DESC");
    return sql;
}

auto SqlModel::getRecordCount() -> int
{
    return getRecordCountWhere(where);
}

auto SqlModel::getRecordCountWhere(const QString &where) -> int
{
    int result = 0;
    QString sql = tr("SELECT count(*) AS records FROM %1").arg(getFrom());
    if (!where.isEmpty()) sql += " WHERE " + where;
    //qDebug() << sql;
    QVariant value = getScholar(sql);
    if (!value.isNull())
        result = value.toInt();
    return result;
}

auto SqlModel::getFieldNames() -> QStringList
{
    QStringList result;
    QSqlQuery query(getSqlSelect(false) + " LIMIT 1 OFFSET 0;");
    if (query.next()) {
        QSqlRecord rs = query.record();
        for (int i = 0; i < rs.count(); ++i)
            result << rs.fieldName(i);
    }
    if (query.lastError().isValid())
        emit sqlError(query.lastQuery(), query.lastError());
    return result;
}

auto SqlModel::getOneFieldValues(const QString &sql) -> QStringList
{
    QStringList result;
    QSqlQuery query(sql);
    while (query.next())
        result << query.value(0).toString();
    if (query.lastError().isValid()) {
        qDebug() << query.lastQuery() << query.lastError();
        emit sqlError(query.lastQuery(), query.lastError());
    }
    return result;
}

auto SqlModel::getScholar(const QString &sql) -> QVariant
{
    QVariant result;
    QSqlQuery query(sql);
    if (query.next())
        result = query.value(0);
    if (query.lastError().isValid())
        emit sqlError(query.lastQuery(), query.lastError());
    return result;
}

auto SqlModel::getSqlUpdateFieldsValues(const int id /* = -1 */) -> QString
{
    QStringList keysValues;
    if (!fieldItems.keys().contains(idName))
        this->setValue(idName , id);
    foreach (const QString &key, fieldItems.keys())
        keysValues << key + "=" + fieldItems[key];

    QString sWhere = (id == -1)? where: tr("%1=%2").arg(idName).arg(id);
    QString result = tr("UPDATE %1 SET %2 WHERE %3").arg(
                tableName, keysValues.join(","), sWhere);
    return result;
}

auto SqlModel::getSqlInsertFieldsValues(const int id /* =-1*/) -> QString
{
    if (id != -1 && !fieldItems.keys().contains(idName))
        this->setValue(idName , id);

    QString result = tr("INSERT INTO %1 (%2) VALUES(%3)").arg(
                tableName, fieldItems.keys().join(","), fieldItems.values().join(","));
    return result;
}

auto SqlModel::saveValues(int id /* =-1*/) -> int
{
    if (id == -1 || getRecordCountWhere(tr("%1=%2").arg(idName).arg(id)) == 0) {
        if (execSql(getSqlInsertFieldsValues(id)) && id == -1)
            return getInsertedId();   //登録されたIDを取得
    } else {
        execSql(getSqlUpdateFieldsValues(id));
    }
    return id;
}

auto SqlModel::getInsertedId() -> int
{
    QString sql;
    if (isPgsql()) sql = tr("SELECT currval('%1_%2_seq')").arg(tableName, idName);
    if (isMysql()) sql = "SELECT LAST_INSERT_ID()";
    if (isSQLite()) sql = tr("SELECT * FROM '%1' WHERE '%2'=last_insert_rowid();").arg(tableName, idName);

    QVariant vValue = SqlModel::getScholar(sql);
    return vValue.isNull()? -1: vValue.toInt();
}

auto SqlModel::getRecord(const int id) -> QSqlRecord
{
    QSqlRecord result;
    QString sql = tr("SELECT * FROM %1 WHERE %2=%3;").arg(getFrom(), idName).arg(id);
    QSqlQuery query(sql);
    if (query.next())
        result = query.record();
    if (query.lastError().isValid())
        emit sqlError(query.lastQuery(), query.lastError());
    return result;
}

auto SqlModel::getValues(const QString &fieldName, const QString &where, const QString &order /*= ""*/) -> QStringList
{
    QString sql;
    sql = tr("SELECT %1 FROM %2 ").arg(fieldName, getFrom());
    if (!where.isEmpty()) sql += " WHERE " + where;
    if (!order.isEmpty()) sql += " ORDER BY " + order;
    return getOneFieldValues(sql);
}

auto SqlModel::getValuesId(const QString &where, const QString &order /*= ""*/) -> QList<int> {
    QList<int> result;
    QStringList isd = getValues(idName, where, order);
    foreach (auto idStr, isd)
        result << idStr.toInt();
    return result;
}

auto SqlModel::execSql(const QString &sql) -> bool
{
    QSqlQuery query;
    bool result = query.exec(sql);
    if (query.lastError().isValid())
        emit sqlError(query.lastQuery(), query.lastError());
    //qDebug() << sql;

    return result;
}

auto SqlModel::execSqlFile(const QString &filePath) -> bool {
    QFile file(filePath);
    if (file.open(QFile::ReadOnly)) {
        QString sql = QString(file.readAll());
        file.close();
        return execSql(sql);
    }
    return false;
}

void SqlModel::deleteTable(const QString &table)
{
    auto deleteName = table.isEmpty()? tableName : table;
    if (isPgsql() || isMysql()) {
        auto fix = isPgsql()? "RESTART IDENTITY" : "";
        execSql(tr("TRUNCATE TABLE %1 %2;").arg(deleteName, fix));
    } else if (isSQLite()) {
        execSql(tr("DELETE FROM %1;").arg(deleteName));
        execSql(tr("DELETE FROM sqlite_sequence WHERE name='%1';").arg(deleteName));
        execSql(tr("vacuum;"));
    }
}

auto SqlModel::getSqlTableDump(const QString &table) -> QStringList
{
    auto result = QStringList();
    auto tableNameOutput = table.isEmpty()? tableName : table;
    auto model = getModel(tr("select * from %1").arg(tableNameOutput));
    if (!model->rowCount()) return result;
    setValueClear();
    for (int i = 0; i < model->rowCount(); i++) {
        auto rs = model->record(i);
        for (int col = 0; col < model->columnCount(); col++)
            setValue(rs.fieldName(col), rs.value(col));
        auto insert = tr("INSERT INTO %1 (%2) VALUES(%3);").arg(tableNameOutput,
                              fieldItems.keys().join(","),
                              fieldItems.values().join(","));
        //qDebug() << insert;
        result << insert;
    }
    return result;
}

auto SqlModel::getModel(const QString &sql) -> QSqlQueryModel *
{
    queryModel->setQuery(sql);
    if (queryModel->lastError().isValid())
        emit sqlError(sql, queryModel->lastError());
    return queryModel;
}
