#include "edit_path_select.h"

#include <QAction>
#include <QIcon>
#include <QFileDialog>

EditPathSelect::EditPathSelect(QWidget *parent) : EditPath(parent)
{
    auto actionSelect = this->addAction(QIcon::fromTheme("document-edit"), QLineEdit::TrailingPosition);
    actionSelect->setToolTip(tr("Select folder"));
    connect(actionSelect, &QAction::triggered, this, &EditPathSelect::on_actionSelect_clicked);

    setReadOnly(false);
}

void EditPathSelect::on_actionSelect_clicked()
{
    QFileDialog::Options options = QFileDialog::DontResolveSymlinks | QFileDialog::ShowDirsOnly
        | QFileDialog::DontUseNativeDialog;
    QString dirName = QFileDialog::getExistingDirectory(this, tr("Select folder"), this->text(), options);
    if (!dirName.isEmpty())
        this->setText(dirName);
}
