#include "edit_path.h"
#include <QApplication>
#include <QAction>
#include <QIcon>
#include <QProcess>

EditPath::EditPath(QWidget *parent) : QLineEdit(parent) {
    auto actionOpen = this->addAction(QIcon::fromTheme("document-open"), QLineEdit::TrailingPosition);
    actionOpen->setToolTip(tr("Open folder"));
    connect(actionOpen, &QAction::triggered, this, [=] () {
        QProcess::startDetached("xdg-open", { text() });
    });
    setReadOnly(true);
}
