#include "edit_pass.h"

#include <QAction>
#include <QIcon>

EditPass::EditPass(QWidget *parent) : QLineEdit(parent)
{
    auto action = this->addAction(QIcon::fromTheme("view-visible"), QLineEdit::TrailingPosition);
    this->setEchoMode(QLineEdit::Password);
    connect(action, &QAction::triggered, this, [=] () {
        if (this->echoMode() == QLineEdit::Normal) {
            this->setEchoMode(QLineEdit::Password);

            action->setIcon(QIcon::fromTheme("view-visible"));
        } else {
            this->setEchoMode(QLineEdit::Normal);
            action->setIcon(QIcon::fromTheme("view-hidden"));
        }
    });
}
