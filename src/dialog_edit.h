#ifndef DIALOG_EDIT_H
#define DIALOG_EDIT_H

#include <QDataWidgetMapper>
#include <QDialog>
#include <QSqlRelationalTableModel>
#include "mainwindow.h"

namespace Ui {
class DialogEdit;
}

class QLineEdit;
class RdpItem;
class Suspend;

class DialogEdit : public QDialog
{
    Q_OBJECT

public:
    explicit DialogEdit(QWidget *parent = nullptr, int id = -1, Host* host = nullptr);
    ~DialogEdit();

    int getId(){ return id; }

private slots:
    void on_buttonBox_accepted();
    void on_editLink_actionClicked();
    void on_editSuspendFile_actionClicked();

private:
    void updateRdpFileEnabled();
    void updateSuspendCtlsEnabled();
    int id;
    QString tableName;
    QString idName;
    bool addMode;
    QSqlRelationalTableModel *model;
    QDataWidgetMapper *mapper;
    RdpItem *rdp;
    Suspend *suspend;
    QList<QLineEdit*> suspendControls;
    MainWindow* main;
    Ui::DialogEdit *ui;
};

#endif // DIALOG_EDIT_H
