#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QApplication>
#include <QMainWindow>
#include <QSystemTrayIcon>

#include "util/base_mainwindow.h"
#include "util/sqlmodel.h"
#include "util/settings.h"
#include "util/host.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class TableViewDelegate;
class QProgressDialog;
class Suspend;

class MainWindow : public BaseMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow() override;

    // setting
    Settings* getSettings() {return settings;}

    // call from scanner
    void openEdit(QWidget *parent = nullptr, int id = -1, Host* data = nullptr);

public slots:
    void onSqlError(const QString &sql , const QSqlError &lastError);

private:
    void openEdit(int id) { openEdit(this, id); }
    void wolCommand(int id);
    void suspendCommand(int id);
    void readSetting();
    void updateNeedActions();
    void showMessage(const QString &title, const QStringList &messages,
        QSystemTrayIcon::MessageIcon icon = QSystemTrayIcon::Critical);
    void downloadOuiFile();
    void checkVersion();
    QStringList getRssVersions(const QString xmlStr);
    double versionCast(const QString &versionStr);

    // setting
    Settings* settings;

    // db
    SqlModel* sqlModel;
    SqlModel* sqlModelView;

    // system tray
    QSystemTrayIcon *trayIcon;
    QMenu *trayIconMenu;
    QAction *actionQuit;

    // timer
    int watingId = -1;
    int stopId = -1;

    QTimer* timerInterval;
    QTimer* timerWakeTimeout;

    // suspend
    Suspend *suspend;

    // other
    bool opneLink;
    int pingWait;
    QList<QAction*> needSelectActions;
    TableViewDelegate *delegate;
    Ui::MainWindow *ui;

private slots:
    void onIntervalTimer();
    void on_tableView_customContextMenuRequested(const QPoint &pos);

    // QWidget interface
protected:
    virtual void closeEvent(QCloseEvent *event) override;
    virtual void showEvent(QShowEvent *event) override;
    virtual void hideEvent(QHideEvent *event) override;
};


#endif // MAINWINDOW_H
