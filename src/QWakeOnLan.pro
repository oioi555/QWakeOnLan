QT += core gui widgets sql network concurrent xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11 #ccache

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

VER = $$system(git describe --abbrev=8 --tags)
VERSTR = '\\"$${VER}\\"'
DEFINES += VERSION=\"$${VERSTR}\"

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

isEmpty(ICONDIR):ICONDIR=/usr/share/icons/hicolor/scalable/apps
isEmpty(APPDIR):APPDIR=/usr/share/applications

icon.path = $$INSTROOT$$ICONDIR
desktop.path = $$INSTROOT$$APPDIR

icon.files = ../etc/QWakeOnLan.svg
desktop.files = ../etc/QWakeOnLan.desktop

INSTALLS += icon desktop

SOURCES += \
    dialog_edit.cpp \
    dialog_login.cpp \
    dialog_scanner.cpp \
    dialog_settings.cpp \
    main.cpp \
    mainwindow.cpp \
    qblowfish/qblowfish.cpp \
    util/base_mainwindow.cpp \
    util/edit_ip.cpp \
    util/edit_mac.cpp \
    util/edit_pass.cpp \
    util/edit_path.cpp \
    util/edit_path_file_select.cpp \
    util/edit_path_select.cpp \
    util/filter_header.cpp \
    util/host.cpp \
    util/rdpItem.cpp \
    util/sortfilterproxymodel.cpp \
    util/sqlmodel.cpp \
    util/suspend.cpp \
    util/tableview_sqlfilter.cpp

HEADERS += \
    dialog_edit.h \
    dialog_login.h \
    dialog_scanner.h \
    dialog_settings.h \
    mainwindow.h \
    qblowfish/qblowfish.h \
    qblowfish/qblowfish_p.h \
    util/base_mainwindow.h \
    util/edit_mac.h \
    util/edit_pass.h \
    util/edit_path.h \
    util/edit_path_file_select.h \
    util/edit_path_select.h \
    util/host.h \
    util/edit_ip.h \
    util/filter_header.h \
    util/menu.h \
    util/rdpItem.h \
    util/settings.h \
    util/sortfilterproxymodel.h \
    util/sqlmodel.h \
    util/suspend.h \
    util/tableview_delegate.h \
    util/tableview_sqlfilter.h

FORMS += \
    dialog_edit.ui \
    dialog_login.ui \
    dialog_scanner.ui \
    dialog_settings.ui \
    mainwindow.ui

TRANSLATIONS += \
    i18n/ja_JP.ts

include(SingleApplication/singleapplication.pri)
DEFINES += QAPPLICATION_CLASS=QApplication

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resource.qrc
