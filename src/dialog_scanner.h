#ifndef DIALOG_SEARCH_H
#define DIALOG_SEARCH_H

#include <QDialog>
#include "mainwindow.h"

namespace Ui {
class DialogScanner;
}

class DialogScanner : public QDialog
{
    Q_OBJECT

public:
    explicit DialogScanner(QWidget *parent = nullptr);
    ~DialogScanner();

private slots:
    void on_tableView_customContextMenuRequested(const QPoint &pos);
    void on_btnClear_clicked();

    void on_btnCopy_clicked();

private:
    void scanHosts();

    SqlModel *sqlModel;
    MainWindow* main;
    Ui::DialogScanner *ui;
};

#endif // DIALOG_SEARCH_H
