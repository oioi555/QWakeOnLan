#ifndef DIALOG_LOGIN_H
#define DIALOG_LOGIN_H

#include <QDialog>

namespace Ui {
class DialogLogin;
}

class Settings;
class DialogLogin : public QDialog
{
    Q_OBJECT

public:
    explicit DialogLogin(const QString &name, const QString &user = "", const QString &pass = "", QWidget *parent = nullptr);
    ~DialogLogin();
    QString user;
    QString pass;
    bool isSave = false;

private slots:
    void on_buttonBox_accepted();

private:
    Settings *settings;
    Ui::DialogLogin *ui;
};

#endif // DIALOG_LOGIN_H
