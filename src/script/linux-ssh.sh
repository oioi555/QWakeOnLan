#!/bin/bash

host=$1
user=$2
pass=$3
port=$4

port_opt=""
if [ -n "$port" ]; then
 port_opt="-p ${port}"
fi

# Linux
sshpass -p $pass ssh $user@$host $port_opt -tt "echo ${pass} | sudo -S systemctl suspend"

exit ${?}
