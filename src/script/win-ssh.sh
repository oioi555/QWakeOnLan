#!/bin/bash

host=$1
user=$2
pass=$3
port=$4

port_opt=""
if [ -n "$port" ]; then
 port_opt="-p ${port}"
fi

# Windows10
# Even if the command succeeds, the process does not end, so forcibly disconnect
timeout 5s sshpass -p $pass ssh $user@$host $port_opt -o ConnectTimeout=3 "powercfg -h off & rundll32.exe powrprof.dll,SetSuspendState 0,1,0"

exit_code=$?

# timeout succeed is 124
if [ $exit_code -eq 124 ]; then
 exit_code=0
fi

exit $exit_code
