# QWakeOnLan
Wake up your devices using WakenOnLan.
Qt application for linux desktop.

+ Wait to wake up and open the RDP link.
    + RDP links ([Remmina](https://remmina.org/), [NoMachine](https://www.nomachine.com/), other)
+ Remote suspend command by ssh.
    + OS (Linux, Windows10)
+ Network scanner to know the MAC address


## Screenshot
|Main Window|Dark Theme|
| :---: | :---: |
| ![main](/uploads/4ef588cf1f6aaf82886173aefd6f16e9/main.png) | ![main-dark](/uploads/3b7f8b13c908666b14c3b68126d00dbc/main-dark.png) |
| ![setting](/uploads/01877272c403a7dd28c2cd669757a25d/setting.png) | <img src="/uploads/a2085397085683150e92610b325ac49f/scanner.png" alt="scanner" height="500"> |

## Requirements
+ [Qt5](https://www.qt.io/licensing/) (>=5.11.3)
+ [SQLite3](https://www.sqlite.org/index.html)
+ [iproute2](https://wiki.linuxfoundation.org/networking/iproute2) (for get MAC address)
+ [sshpass](https://sourceforge.net/projects/sshpass/) (for Remote suspend command)
+ **Remote desktop client** (optional)
    + [Remmina](https://remmina.org/)
    + [NoMachine](https://www.nomachine.com/)

## Install
You can download and install packages from the [release page](https://gitlab.com/oioi555/QWakeOnLan/-/releases).

## Compile
### Arch Linux (Manjaro etc.) 
```
git clone https://gitlab.com/oioi555/QWakeOnLan.git/
cd ./QWakeOnLan/setup/arch/
./install.sh
```
### Fedora 34
```
git clone https://gitlab.com/oioi555/QWakeOnLan.git/
cd ./QWakeOnLan/setup/
./fedora.sh
```
### Debian 10（Buster）Ubuntu 20.04 LTS（Linux mint 20）
```
git clone https://gitlab.com/oioi555/QWakeOnLan.git/
cd ./QWakeOnLan/setup/
./debian.sh
```
## License
[GNU General Public License v2.0.](LICENSE)

