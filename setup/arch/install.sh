#!/bin/bash

# makepkgに必要
sudo pacman -S --noconfirm base-devel git

rm -rf build pkg
 
# makepkg option
# -f パッケージの上書き
# -i パッケージ作成後インストール
# -c パッケージのクリーンナップ
# -s 依存パッケージインストール
makepkg -fics

rm -rf build
