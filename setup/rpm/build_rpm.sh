#!/bin/sh

source="QWakeOnLan"
build_dir="/builds/oioi555/${source}"
version=$(git describe --abbrev=8 --tags | grep -o v[^-]*)
spec="${source}-git.spec"

if [ -d ${build_dir} ]; then
    # gitlab ci 
    cp "./setup/rpm/${source}-git.spec" /root
    sed -i -e "s/_VERSION_/${version}/g" /root/$spec
    cd /root
else
    # ローカルでRPM作成
    sudo dnf install -y https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
    sudo dnf install rpmdevtools rpm-build
    sudo dnf builddep -y $spec

    rm -rf build
    mkdir build
    cd build
    
    mkdir -p ./rpmbuild/{BUILD,RPMS,SOURCES,SPECS,SRPMS}
    cp ../$spec rpmbuild/SPECS
    spec="rpmbuild/SPECS/${spec}"
    sed -i -e "s/_VERSION_/${version}/g" $spec
    
    build_dir="../../.."
fi

rm -rf $source
# ソースフォルダを作る
mkdir $source

# ソースのコピー
cp -r $build_dir/src $source
cp -r $build_dir/etc $source
cp -r $build_dir/.git $source
cp $build_dir/LICENSE $source

# ソースの圧縮
tar cfvz "${source}.tar.gz" $source
cp "${source}.tar.gz" ./rpmbuild/SOURCES

spectool -g -R $spec
rpmbuild --define "_topdir $(pwd)/rpmbuild" -ba $spec
