%define version _VERSION_
Name: QWakeOnLan
Version: %{version}
Release: master%{?dist}
Summary: Wake up your devices using WakenOnLan.

License: GPLv2
URL: https://gitlab.com/oioi555/QWakeOnLan
Source0: %{name}.tar.gz

BuildRequires:	mpv-libs-devel
BuildRequires:	qt5-qtbase-devel
BuildRequires:	pkg-config
BuildRequires:	gcc-c++
BuildRequires:	make
BuildRequires:	git

Requires:	qt5-qtsvg
Requires:	qt5-qttranslations
Requires:	iproute
Requires:	sshpass
Requires:	breeze-icon-theme

Provides:	QWakeOnLan = %{version}-%{release}

%define debug_package %{nil}

%description
Wake up your devices using WakenOnLan.

%prep
rm -rf ${RPM_BUILD_ROOT}
%setup -q -n %{name}
qmake-qt5 ./src CONFIG+=release -spec linux-g++

%build
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
INSTALL_ROOT=$RPM_BUILD_ROOT make install

mkdir -p %{buildroot}%{_datadir}/applications
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/scalable/apps/

cp %{_builddir}/%{name}/etc/%{name}.desktop %{buildroot}%{_datadir}/applications
cp %{_builddir}/%{name}/etc/%{name}.svg %{buildroot}%{_datadir}/icons/hicolor/scalable/apps

%clean
rm -rf $RPM_BUILD_ROOT

%files
%license LICENSE
/opt/%{name}/bin/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/scalable/apps/%{name}.svg

%changelog

