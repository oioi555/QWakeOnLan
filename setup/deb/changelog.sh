#!/bin/bash

project="qwakeonlan"

# Get list of tags (order asc)
gitTags=( $(git tag) )
tagsCount=${#gitTags[@]}
if (( tagsCount == 0 )); then
   exit 0
fi

# Get Hash list from oldest Tag to HEAD
hashes=( $(git log --format="%h" ${gitTags[0]}..HEAD) )

# Count the number of commits per Tag
tagsIndex=$(( tagsCount - 1 ))
positionCounts=()
count=0
for hash in ${hashes[@]}; do
    tagLog=$( git log -n 1 --pretty=format:"%d" $hash )
    if [[ $tagLog == *${gitTags[$tagsIndex]}* ]]; then
        tagsIndex=$(( --tagsIndex ))
        positionCounts=( $count ${positionCounts[@]} )
        count=0
    else
        count=$(( ++count ))
    fi
done
if (( tagsCount != ${#positionCounts[@]} )); then
    positionCounts=( $((count -1)) ${positionCounts[@]} )
fi

# output
tagsIndex=$(( tagsCount - 1 ))
position=${positionCounts[ ${#positionCounts[@]} -1 ]}
for hash in ${hashes[@]}; do
    version=${gitTags[$tagsIndex]}
    version=${version##v}
    printf "$project ($version-$position) unstable; urgency=medium\n\n"
    git log -n 1 --pretty=format:"  * %s%n%n" $hash
    git log -n 1 --pretty=format:" -- %an <%ae>  %cd%n%n" $hash --date=rfc

    tagLog=$( git log -n 1 --pretty=format:"%d" $hash )
    if [[ $tagLog == *${gitTags[$tagsIndex]}* ]]; then
        tagsIndex=$(( --tagsIndex ))
        position=${positionCounts[$tagsIndex]}
    else
        position=$(( --position ))
    fi
done
exit 0
