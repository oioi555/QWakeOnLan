#!/bin/sh

source="qwakeonlan"
build_dir="/builds/oioi555/QWakeOnLan"

if [ ! -d ${build_dir} ]; then
    # ビルドに必要なもの
    sudo apt install -y git build-essential qt5-default qt5-qmake \
    libqt5sql5-sqlite libqt5svg5 libqt5network5 qttranslations5-l10n\
    qttranslations5-l10n pkg-config debhelper fakeroot \
    breeze-icon-theme
fi

rm -rf $source

# ソースフォルダを作る
mkdir $source

# ソースのコピー
cp -r ../../src $source
cp -r ../../etc $source
cp -r ../../.git $source
cp -r ./debian $source

cd $source

# 実行ファイルの展開先を変更
sed -i -e "s/\/opt\//\$\${INSTROOT}\/opt\//g" src/QWakeOnLan.pro

# 変更履歴の簡易生成(ログの最新バージョンでdebが作成される)
../changelog.sh > debian/changelog

dpkg-buildpackage -rfakeroot -b -us -uc
