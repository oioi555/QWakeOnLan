#!/bin/sh

sudo dnf install git gcc gcc-c++ make qt5-qtbase qt5-qtbase-devel qt5-qtsvg pkg-config

rm -rf build
mkdir build
cd build

qmake-qt5 ../../src CONFIG+=release -spec linux-g++

make -j $(nproc)
sudo make install

# アイコンの強制更新
sudo gtk-update-icon-cache /usr/share/icons/hicolor
cd ..
