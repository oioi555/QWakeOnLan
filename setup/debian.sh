#!/bin/sh

sudo apt install -y git build-essential qt5-default qt5-qmake\
    libqt5sql5-sqlite libqt5network5 libqt5svg5 qttranslations5-l10n\
    iproute2 sshpass pkg-config

rm -rf build
mkdir build
cd build

/usr/lib/x86_64-linux-gnu/qt5/bin/qmake ../../src CONFIG+=release -spec linux-g++

make -j $(nproc)
sudo make install

# アイコンの強制更新
sudo gtk-update-icon-cache /usr/share/icons/hicolor
cd ..
